import React from "react";
import { Container, Grid, Typography, Link } from "@material-ui/core";

const COMMUNITY_LINKS = [
	{
		title: "Github", 
		href: "",
	},
	{
		title: "Twitter", 
		href: "",
	},
	{
		title: "Stackoverflow", 
		href: "",
	},
	{
		title: "Team", 
		href: "",
	},
];

const RESOURCES_LINKS = [
	{
		title: "Support", 
		href: "",
	},
	{
		title: "Blog", 
		href: "",
	},
	{
		title: "Material Icons", 
		href: "",
	},
];

// Props received from selectors
export interface IDataProps {}

// Props received from dispatchers
export interface ICallbackProps {}

// External props passed from parent component
export interface IExternalProps {}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {}

export default class FooterUI extends React.PureComponent<Props, ILocalState> {
  public render() {
    return (
      <Container component="div" maxWidth="md">
        <footer className="footer__offset">
          <Grid container spacing={2}>
            <Grid item xs={12} md={3}>
              <div className="footer__company">
                <Typography component="p" variant="body1" color="textPrimary">
                  Company Name
                </Typography>
              </div>
            </Grid>
            <Grid item xs={12} md={3}>
				<div>
					<Typography component="h3" variant="body1" gutterBottom>
						Community
					</Typography>
					<ul className="footer__links">
						{COMMUNITY_LINKS.map((link, index) => {
							return (
								<li key={`${link.title}-${index}`} className="footer__link">
									<Typography variant="body2">
										<Link href={link.href} color="inherit">
											{link.title}
										</Link>
									</Typography>
								</li>
							);
						})}
					</ul>
				</div>
            </Grid>
            <Grid item xs={12} md={3}>
              <div>
			  	<Typography component="h3" variant="body1" gutterBottom>
                    Resources
                </Typography>
				<ul className="footer__links">
					{RESOURCES_LINKS.map((link, index) => {
						return (
							<li key={`${link.title}-${index}`} className="footer__link">
								<Typography variant="body2">
									<Link href={link.href} color="inherit">
										{link.title}
									</Link>
								</Typography>
							</li>
						);
					})}
				</ul>
              </div>
            </Grid>
          </Grid>
          <Typography component="p" variant="body2" color="textSecondary">
            Webdesign 2019
          </Typography>
        </footer>
      </Container>
    );
  }
}
