import React from "react";
import { Container, Typography, Grid } from "@material-ui/core";
import NavbarUI from "ui/molecule/Navbar/NavbarUI";
import { User } from "ui/organism/User/state";
import { History } from "history";

export interface IDataProps {
    user: User;
}

export interface IExternalProps {
	history: History;
}

type Props = IDataProps & IExternalProps;

export default class PasswordResetUI extends React.PureComponent<Props> {

    public render() {
        const {user, history} = this.props;
        return (
            <React.Fragment>
                <NavbarUI user={user} history={history}/>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        Nazov formularu
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        Form na zadanie emailu
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        Instrukcie ze ako
                    </Grid>
                </Grid>
            </React.Fragment>
        );
  }
}
