import {connect} from "react-redux";
// import dispatchers from "ui/organism/User/dispatchers";
import selectors from "ui/molecule/PasswordReset/selectors";
import PasswordResetUI, {IDataProps, IExternalProps} from "ui/molecule/PasswordReset/PasswordResetUI";

export default connect<IDataProps, null, IExternalProps>(
    selectors,
    null,
)(
    PasswordResetUI,
);
