import { createStructuredSelector } from "reselect";
import { IDataProps, IExternalProps } from "ui/molecule/PasswordReset/PasswordResetUI";
import { userStateSelector } from "ui/organism/User/selectors";

export default createStructuredSelector<unknown, IExternalProps, IDataProps>({
    user: userStateSelector,
});
