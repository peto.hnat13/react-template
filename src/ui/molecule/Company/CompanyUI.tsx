import React from "react";
import { Container, Typography } from "@material-ui/core";

// Props received from selectors
export interface IDataProps {}

// Props received from dispatchers
export interface ICallbackProps {}

// External props passed from parent component
export interface IExternalProps {}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {}

export default class CompanyUI extends React.PureComponent<Props, ILocalState> {
  public render() {
    return (
		<section>
			<Container className="company__container" component="div" maxWidth="md">
				<div>
					<Typography 
						component="h1" 
						variant="h3" 
						color="inherit" 
						gutterBottom 
						className="company__title"
					>
						COMPANY NAME
					</Typography>
					<Typography component="h2" variant="h5" color="inherit">
						React components for faster and easier web development. Build your own design system, or start with Material Design.
					</Typography>
				</div>
			</Container>
		</section>
    );
  }
}
