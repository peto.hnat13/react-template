import React, { Component } from "react";
import { Container, Typography, Grid } from "@material-ui/core";
import PriceCardUI from "ui/atom/PriceCard/PriceCardUI";

const CARDS_DATA = [
    {
        planLabel: "Free",
        planPrice: 22,
        planDuration: "1 Month",
        planInfo: [
            "10 GB Free",
            "Always online",
        ],
        btnLabel: "Sign Up"
    },
    {
        planLabel: "Premium",
        planPrice: 55,
        planDuration: "1 Year",
        planInfo: [
            "1 TB Free",
            "Always online",
            "Support",
            "Everything",
        ],
        btnLabel: "Buy Now"
    },
    {
        planLabel: "Basic",
        planPrice: 10,
        planDuration: "1 Day",
        planInfo: [
            "100 GB Free",
            "Always online",
        ],
        btnLabel: "Buy Now"
    },
];

// Props received from selectors
export interface IDataProps {}

// Props received from dispatchers
export interface ICallbackProps {}

// External props passed from parent component
export interface IExternalProps {}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {};

export default class PricingUI extends React.PureComponent<Props, ILocalState> {

  public render() {
    return (
        <section>
            <Container component="div" maxWidth="sm" className="pricing__container">
                <Typography component="h1" variant="h2" color="textPrimary" align="center" gutterBottom>
                    Pricing
                </Typography>
                <Typography component="p" variant="h5" color="textSecondary" align="center">
                    Quickly build an effective pricing table for your potential customers with this layout. It's built with default Material-UI components with little customization.
                </Typography>
            </Container>
            <Container component="div" maxWidth="md">
                <Grid container spacing={5} alignItems="flex-end">
                    {CARDS_DATA.map((plan, index) => (
                        <Grid key={index} item xs={12} sm={6} md={4}>
                            <PriceCardUI
                                planLabel={plan.planLabel}
                                planPrice={plan.planPrice}
                                planDuration={plan.planDuration}
                                planInfo={plan.planInfo}
                                btnLabel={plan.btnLabel}
                            />
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </section>
    );
  }
}