import React, { ChangeEvent} from "react";
import { createStyles, Theme } from "@material-ui/core/styles";
import { Button, Dialog, AppBar, Toolbar, IconButton, Typography, Slide } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { TransitionProps } from "@material-ui/core/transitions";
import { TextField, Container, Box, Paper } from "@material-ui/core";
import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme();
export const customStyles = createStyles({
    appBar: {
        position: "relative",
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
});
const Transition = React.forwardRef<unknown, TransitionProps>(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

interface CSSStyles {
    classes: {
        appBar: string;
        title: string;
        textField: string;
    };
    theme: Theme; // TODO ak chceme pouzit temu v komponente
}

export interface IExternalProps {
    open: boolean;
    handleClose(): void;
    handleSubmit(body: string): void;
};

interface ILocalState {
	body: string;
	errors: null | string[];
}

type Props = IExternalProps & CSSStyles;

export default class BabySitterDialogUI extends React.PureComponent<Props, ILocalState> {
    public state = {
        body: "",
        errors: null,
    };

    public render() {
        const {open, handleClose, classes, theme} = this.props;

        return (
            <div>
                <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                    <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Create New Baby Sitter
                        </Typography>
                        <Button color="inherit" onClick={this.onSubmit}>
                            save
                        </Button>
                    </Toolbar>
                    </AppBar>
                    <Container component="div" maxWidth="sm">
					    <Box component="div" className="form__box">
						    <Paper elevation={0} className="form__paper form__paper--color">
                                <form>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="standard-body"
                                        label="Body"
                                        name="body"
                                        className={classes.textField}
                                        value={this.state.body}
                                        onChange={this.onChange}
                                        margin="normal"
                                    />
                                </form>
                            </Paper>
					    </Box>
				    </Container>
                </Dialog>
            </div>
        );
    }

    public onChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const target = event.target;
        const fieldValue = target.value;
        const fieldName = target.name;

        switch(fieldName) {
            case "body":
                this.setState({
                    body: fieldValue,
                });  
            default:
                return;
        }
    }

    public onSubmit = (event: any) => {
        if (this.state.body.length) {
            this.props.handleSubmit(this.state.body);
        }
    }
}
