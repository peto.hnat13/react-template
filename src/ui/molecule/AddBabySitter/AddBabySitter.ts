import {connect} from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import AddBabySitterUI, {customStyles, IExternalProps} from "ui/molecule/AddBabySitter/AddBabySitterUI";

// alebo takto temu mame v props export default withStyles(customStyles, { withTheme: true })(AddBabySitterUI);
export default withStyles(customStyles, { withTheme: true })(
    connect<null, null, IExternalProps>(
        null,
        null,
    )(
        AddBabySitterUI,
    )
);
