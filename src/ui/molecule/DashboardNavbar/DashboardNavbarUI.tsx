import classnames from "classnames";
import React from "react";
import {
    Typography,
    Toolbar,
    ListItemText,
    ListItemIcon,
    ListItem,
    List,
    IconButton,
    Divider,
    CssBaseline,
    AppBar,
} from "@material-ui/core";
import { Theme, createMuiTheme, createStyles } from "@material-ui/core/styles";
import { Link, RouteComponentProps } from "react-router-dom";
import { ROUTE } from "app/routes";
import { User } from "ui/organism/User/state";
import {
    HomeRounded,
    PowerSettingsNewRounded,
    SearchRounded,
} from "@material-ui/icons";
import MenuIcon from "@material-ui/icons/Menu";
import { SwipeableDrawer } from "@material-ui/core";

const theme = createMuiTheme();
export const customStyles = createStyles({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    toolbar: theme.mixins.toolbar,
    toolbarWidth: {
        minWidth: 216,
    },
    title: {
        flexGrow: 1,
    },
});

export interface IExternalProps extends RouteComponentProps<any> {
    loggedUser: User;
    onUserLogout(): void;
};

interface CSSStyles {
    classes: {
        menuButton: string;
        toolbar: string;
        toolbarWidth: string;
        title: string;
    };
    theme: Theme; // TODO ak chceme pouzit temu v komponente
}

type IProps = IExternalProps & CSSStyles;

interface ILocalState {
    isOpen: boolean;
}

export default class DashboardNavbarUI extends React.PureComponent<IProps, ILocalState> {
    public state = {
        isOpen: false,
    };

    public render() {
        const {classes, loggedUser} = this.props;

        const drawer = (
            <div>
                <div className={classnames(
                    classes.toolbar,
                    classes.toolbarWidth,
                )} />
                <Divider />
                <List>
                    <ListItem color="inherit" component={Link} to={ROUTE.HOME} button key="home" onClick={this.handleDrawerToggle}>
                        <ListItemIcon><HomeRounded /></ListItemIcon>
                        <ListItemText primary={"Home"}/>
                    </ListItem>
                    <ListItem color="inherit" component={Link} to={ROUTE.DASHBOARD} button key="search" onClick={this.handleDrawerToggle}>
                        <ListItemIcon><SearchRounded /></ListItemIcon>
                        <ListItemText primary={"Search"}/>
                    </ListItem>
                </List>
                <Divider />
                <List>
                    <ListItem button key="Logout" onClick={this.handleLogout}>
                        <ListItemIcon><PowerSettingsNewRounded /></ListItemIcon>
                        <ListItemText primary="Logout"/>
                    </ListItem>
                </List>
            </div>
        );

        return (
            <div>
                <CssBaseline />
                <AppBar position="fixed">
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            edge="start"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" noWrap className={classes.title}>
                            {loggedUser.role === "admin" ? "Administration" : "Company Name"} 
                        </Typography>
                        <Typography variant="subtitle1" noWrap>
                            {loggedUser.email}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <SwipeableDrawer
                    open={this.state.isOpen}
                    onClose={this.handleDrawerToggle}
                    onOpen={this.handleDrawerToggle}
                >
                    {drawer}
                </SwipeableDrawer>
            </div>
        );
    }

    public handleDrawerToggle = () => {
        this.setState({
            isOpen: !this.state.isOpen,
        });
    };

    public handleLogout = () => {
		if (this.props.onUserLogout) {
            this.props.onUserLogout();
			this.props.history.push(ROUTE.LOGIN);
		}
	};
}
