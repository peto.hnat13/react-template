import { withRouter } from "react-router";
import { withStyles } from '@material-ui/core/styles';
import DashboardNavbarUI, { customStyles } from "ui/molecule/DashboardNavbar/DashboardNavbarUI";

// alebo takto temu mame v props export default withStyles(customStyles, { withTheme: true })(AddBabySitterUI);
export default withStyles(customStyles, { withTheme: true })(withRouter(DashboardNavbarUI));
