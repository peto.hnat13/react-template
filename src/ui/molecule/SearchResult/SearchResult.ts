import {connect} from "react-redux";
import { withRouter } from "react-router";
// import dispatchers from "ui/organism/User/dispatchers";
// import selectors from "ui/molecule/PasswordReset/selectors";
import SearchResultUI, {IExternalProps, customStyles} from "ui/molecule/SearchResult/SearchResultUI";
import {withStyles} from "@material-ui/core/styles";

export default withRouter(
    connect<any, any, IExternalProps>(
    null,
    null,
    )(
        withStyles(customStyles, { withTheme: true })(SearchResultUI)
    ),
)
