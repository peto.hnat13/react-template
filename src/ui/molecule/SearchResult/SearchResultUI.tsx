import React from "react";
import {createStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {Divider} from "@material-ui/core";
import {ROUTE} from "app/routes";
import {Link, RouteComponentProps} from "react-router-dom";
import { GISABabysitterResponse } from "ui/organism/BabySitter/BabySitterResource";

export const customStyles = createStyles({
    babysitterContainer: {
        paddingTop: 40,
        marginBottom: 40,
    },
    babysitterId: {
        marginBottom: 8,
    },
    babysitterName: {
        marginBottom: 12,
    },
    label: {
        marginRight: 12,
    },
    detailLink: {
        textDecoration: "none",
    },
    detailButton: {
        marginTop: 24,
    } 
});

export interface IExternalProps extends RouteComponentProps<any> {
    businessId: string;
    searchRecord: GISABabysitterResponse;
}

export interface CSSStyles {
    classes: {
        babysitterContainer: string;
        babysitterId: string;
        babysitterName: string;
        label: string;
        detailLink: string;
        detailButton: string;
    };
}

type IProps = IExternalProps & CSSStyles;

export default class SearchResultUI extends React.PureComponent<IProps> {
    public render() {
        const {businessId, classes, searchRecord} = this.props;

        return (
            <>
                <div className={classes.babysitterContainer}>
                    <Typography variant="h5" component="h5" color="textPrimary" className={classes.babysitterId}>
                        <Typography variant="body2" component="span" color="textSecondary" className={classes.label}>Business ID:</Typography>
                        {businessId}
                    </Typography>
                    <Typography variant="h6" component="h6" color="textPrimary" className={classes.babysitterName}>
                        <Typography variant="body2" component="span" color="textSecondary" className={classes.label}>Fullname:</Typography>
                        {searchRecord.name || "N/A"}
                    </Typography>
                    <Typography variant="subtitle1" component="p" color="textPrimary">
                        <Typography variant="body2" component="span" color="textSecondary" className={classes.label}>Registration Country:</Typography>
                        Austria, AT
                    </Typography>
                    <Link to={{
                            pathname: `${ROUTE.BABYSITTERS}/${businessId}`,
                            state: {
                                businessId,
                                from: this.props.location,
                            }
                        }}
                        className={classes.detailLink}
                    >
                        <Button color="primary" key="detail" className={classes.detailButton} size="large" variant="outlined">
                            Go to Detail
                        </Button>
                    </Link>
                </div>
                <Divider/>
            </>
        )
    }
}
