import React from "react";
import { Container, Typography, Grid } from "@material-ui/core";
import SectionTitleUI from "ui/atom/SectionTitle/SectionTitleUI";

// Props received from selectors
export interface IDataProps {}

// Props received from dispatchers
export interface ICallbackProps {}

// External props passed from parent component
export interface IExternalProps {}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {}

export default class HowItWorksUI extends React.PureComponent<Props, ILocalState> {
  public render() {
    return (
		<section>
            <Container component="div" maxWidth="lg" className="works__container">
                <SectionTitleUI label="How It Works"/>
                <div>
                    <Grid container spacing={5}>
                        {[0, 1, 2].map((item, index) => (
                            <Grid key={`${item}-${index}`} item xs={12} md={4}>
                                <div className="works__step">
                                    <div className="works__step-title">{index + 1}.</div>
                                    {/* <img src="" alt="" className="works__step-image"/> */}
                                    <Typography component="h3" variant="h5" color="inherit" align="center">
                                        New offers every week. New experiences, new surprises. Your Sundays will no longer be alike.
                                    </Typography>
                                </div>
                            </Grid>
                        ))}
                    </Grid>
                </div>
			</Container>
		</section>
    );
  }
}
