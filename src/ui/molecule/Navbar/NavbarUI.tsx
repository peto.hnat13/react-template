import React from "react";
import {
  AppBar,
  Button,
  Toolbar,
  Typography,
  IconButton,
  Menu,
  MenuItem
} from "@material-ui/core";
import { Link } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import {User} from "ui/organism/User/state";
import {History} from "history";
import { ROUTE } from "app/routes";

// Props received from selectors
export interface IDataProps {}

// Props received from dispatchers
export interface ICallbackProps {}

// External props passed from parent component
export interface IExternalProps {
	user: User;
	history: History
	onUserLogout?: () => void;
}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {
	anchorEl: null | HTMLElement;
	isOpen: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  	createStyles({
		menuButton: {
			marginRight: theme.spacing(2),
		},
		title: {
			flexGrow: 1,
		},
  	}),
);

export default class NavbarUI extends React.PureComponent<Props, ILocalState> {
	public state: ILocalState = {
		anchorEl: null,
		isOpen: false,
	};

	public render() {
		// TODO replace with real user data
		const {user, history} = this.props;
		
		return (
			<AppBar>
				{user.isLogged && history.location.pathname === ROUTE.HOME &&
					this.renderLoggedTollbar() 
				}
				{!user.isLogged &&
					this.renderUnloggedToolbar() 
				}
			</AppBar>
		);
	}

	public renderLoggedTollbar = () => {
		//const classes = useStyles();
		
		return (
			<Toolbar>
				<Typography variant="h6" className="navbar__title">
					<Link to={ROUTE.DASHBOARD} color="inherit" className="navbar__link">
						Dashboard
          			</Link>	
				</Typography>
				<div>
					<IconButton
						aria-label="account of current user"
						aria-controls="menu-appbar"
						aria-haspopup="true"
						onClick={this.handleMenu}
						color="inherit"
					>
						<AccountCircle />
					</IconButton>
					<Menu
						id="menu-appbar"
						anchorEl={this.state.anchorEl}
						anchorOrigin={{
							vertical: "top",
							horizontal: "right"
						}}
						keepMounted
						transformOrigin={{
							vertical: "top",
							horizontal: "right"
						}}
						open={this.state.isOpen}
						onClose={this.handleClose}
					>
						{/*<MenuItem onClick={this.handleProfile}>Profile</MenuItem>*/}
						<MenuItem onClick={this.handleLogout}>Logout</MenuItem>
					</Menu>
				</div>
			</Toolbar>
		);
	};

  	public handleMenu = (event: React.MouseEvent<HTMLElement>) => {
		this.setState({
			anchorEl: event.currentTarget,
			isOpen: true,
		});
  	};

	public handleClose = () => {
		this.setState({
			anchorEl: null,
			isOpen: false,
		});
  	};

	// public handleProfile = () => {
	// 	this.props.history.push(ROUTE.PROFILE);
	// }

	public handleLogout = () => {
		if (this.props.onUserLogout) {
			this.props.onUserLogout();
			this.props.history.push(ROUTE.LOGIN);
		}
	};

  	public renderUnloggedToolbar = () => {
		return (
			<Toolbar>
				<Typography variant="h6" color="inherit" className="navbar__title">
				<Link to="/" color="inherit" className="navbar__link">
					Company Name
				</Link>
				</Typography>
				<Button color="inherit" component={Link} to="/login">
					Login
				</Button>
				<Button color="inherit" component={Link} to="/signup">
					Sign up
				</Button>
			</Toolbar>
		);
	};
}
