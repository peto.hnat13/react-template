import React from 'react';
import {Theme, createStyles, withStyles, createMuiTheme} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import {Search, Close} from '@material-ui/icons';
import {connect} from "react-redux";
import CircularProgress from '@material-ui/core/CircularProgress';

const theme = createMuiTheme();
export const customStyles = createStyles({
    root: {
        padding: '4px 8px',
        display: 'flex',
        alignItems: 'center',
        width: "100%",
        marginBottom: 28,
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
        height: 32,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        height: 28,
        margin: 2,
    },
    progress: {
        padding: 6,
        margin: 2,
    }
});

interface IExternalProps {
    dataLoaded: boolean;
    isLoading: boolean;
}

interface ICallbackProps {
    onSubmitSearch(phrase: string): void;
    onSearchResultLoading(): void;
}

interface CSSStyles {
    classes: {
        root: string;
        input: string;
        iconButton: string;
        divider: string;
        progress: string;
    };
    theme: Theme;
}

type IProps = IExternalProps & ICallbackProps & CSSStyles;

interface ILocalState {
    searchPhrase: string;
}

class SearchBarUI extends React.PureComponent<IProps, ILocalState> {
    public state = {
        searchPhrase: "",
    };
    private inputRef = React.createRef<SearchBarUI>();


    public render() {
        const {classes, isLoading} = this.props;

        return (
            <Paper component="form" className={classes.root} onSubmit={this.onSubmit}>
                <InputBase
                    className={classes.input}
                    placeholder="Type Business ID and press Enter"
                    inputProps={{ "aria-label": "Type Business ID and press Enter" }}
                    onChange={this.onChange}
                    value={this.state.searchPhrase}
                    autoFocus={true}
                    ref={this.inputRef}
                />
                {isLoading ?
                    <CircularProgress className={classes.progress}/>
                :
                    this.state.searchPhrase.length ?
                        <IconButton type="button" className={classes.iconButton} aria-label="clear" onClick={this.onClear}>
                            <Close />
                        </IconButton>
                    :
                        <IconButton
                            type="submit"
                            className={classes.iconButton}
                            aria-label="search"
                        >
                            <Search />
                        </IconButton>
                }
            </Paper>
        );
    }

    private onChange = (event: any) => {
        this.setState({
            searchPhrase: event.target.value,
        });
    }

    private onClear = (event: any) => {
        event.preventDefault();
        this.setState({
            searchPhrase: "",
        });
        // TODO po clearnuti focusnut input mozno
    }

    private onSubmit = (event: any) => {
        event.preventDefault();
        this.props.onSubmitSearch(this.state.searchPhrase);
        this.props.onSearchResultLoading();
    }
}

export default withStyles(customStyles, { withTheme: true })(
    connect<null, null, IExternalProps>(
        null,
        null,
    )(
        SearchBarUI,
    )
);
