import React from "react";
import { Button, Snackbar, IconButton, Icon} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';
import { SnackbarOrigin } from "@material-ui/core/Snackbar";
import clsx from "clsx";
import { createStyles, Theme } from "@material-ui/core/styles";
import { theme } from "app/theme";
import { green, amber, red } from '@material-ui/core/colors';

export const customStyles = createStyles({
    // success: {
    //     backgroundColor: green[600],
    // },
    // error: {
    //     backgroundColor: red[500],
    // },
    // info: {
    //     backgroundColor: theme.palette.primary.main,
    // },
    // warning: {
    //     backgroundColor: amber[700],
    // },
    // icon: {
    //     fontSize: 20,
    // },
    // iconVariant: {
    //     opacity: 0.9,
    //     marginRight: theme.spacing(1),
    // },
    message: {
        display: "flex",
        alignItems: "center",
    },
    close: {
        padding: theme.spacing(0.5),
    },
});

export interface IExternalProps {
    type?: "success" | "warning" | "error" | "info";
    message: string;
    duration: number; // ms
    position?: SnackbarOrigin;
    className?: string;
    isOpen: boolean;
    onSnacbarVisibilityChange(isOpen: boolean): void;
}

export interface CSSStyles {
    classes: {
        // icon: string;
        // iconVariant: string;
        close: string;
    };
    theme: Theme;
}

type IProps = IExternalProps & CSSStyles;

const DEFAULT_VERTICAL_POSITION = "bottom";
const DEFAULT_HORIZONTAL_POSITION = "center";
// const ICON_VARIANT = {
//     success: CheckCircleIcon,
//     warning: WarningIcon,
//     error: ErrorIcon,
//     info: InfoIcon,
// };

export default class SnackbarUI extends React.PureComponent<IProps> {
    public render() {
        const {type, message, position, classes, className, duration, theme} = this.props;
        const verticalPosition = position && position.vertical || DEFAULT_VERTICAL_POSITION;
        const horizontalPosition = position && position.horizontal || DEFAULT_HORIZONTAL_POSITION;
        // const Icon = ICON_VARIANT[type]; // TODO nefunguju ikonky
        
        return (
            <div>
                <Snackbar
                    className={className}
                    anchorOrigin={{
                        vertical: verticalPosition,
                        horizontal: horizontalPosition,
                    }}
                    open={this.props.isOpen}
                    autoHideDuration={duration}
                    onClose={this.handleClose}
                    ContentProps={{
                        "aria-describedby": "message-id",
                    }}
                    message={
                        <span id="message-id">
                            {/* <Icon className={clsx(classes.icon, classes.iconVariant)} /> */}
                            {message}
                        </span>
                    }
                    action={[
                        // <Button key="undo" color="secondary" size="small" onClick={this.handleClose}>
                        //     UNDO
                        // </Button>,
                        <IconButton
                            key="close"
                            aria-label="close"
                            color="inherit"
                            className={classes.close}
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                    ]}
                />
            </div>
        )
    }

    private handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
        if (reason === "clickaway") {
            return;
        }

        this.props.onSnacbarVisibilityChange(false);
    }
}
