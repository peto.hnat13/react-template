import classnames from "classnames";
import React from "react";

export interface IExternalProps {
    message: string;
    type: "success" | "info" | "error";
}

type Props = IExternalProps;

export default function Form(props: Props) {
    return (
        <div 
            className={classnames(
                "formMessage__container",
                {
                    "formMessage__success": props.type === "success",
                    "formMessage__info": props.type === "info",
                    "formMessage__error": props.type === "error",
                },
        )}>
            {props.message}
        </div>
    )
}
