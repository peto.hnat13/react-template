import classnames from "classnames";
import React from "react";
import { APP_CONFIG } from "app/config";


export interface IExternalProps {
    message: string;
    type: string;
}

type Props = IExternalProps;

export default function Form(props: Props) {
    return (
        <div className={classnames(
            "formMessage__container", 
            {
                "formMessage__success": props.type === APP_CONFIG.FORM.MESSAGE.TYPES.SUCCESS,
                "formMessage__info": props.type === APP_CONFIG.FORM.MESSAGE.TYPES.INFO,
                "formMessage__error": props.type === APP_CONFIG.FORM.MESSAGE.TYPES.ERROR,
            }
        )}>
            {props.message}
        </div>
    )
}
