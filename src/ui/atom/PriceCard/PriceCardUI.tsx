import React, { Component } from "react";
import {Paper, Typography, Card, CardHeader, CardContent, CardActions, Button } from "@material-ui/core";

// Props received from selectors
export interface IDataProps {}

// Props received from dispatchers
export interface ICallbackProps {}

// External props passed from parent component
export interface IExternalProps {
    planLabel: string;
    planPrice: number;
    planDuration: string,
    planInfo: string[];
    btnLabel: string;
}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {};

export default class PriceCardUI extends React.PureComponent<Props, ILocalState> {

    public render() {
        const {
            planLabel,
            planPrice,
            planDuration,
            planInfo,
            btnLabel,
        } = this.props;

        return (
            <Paper component="div" elevation={5} square={false}>
                <Card>
                    <CardHeader
                        className="card__header"
                        title={planLabel}
                    />
                    <CardContent>
                        <div className="card__price">
                            <Typography component="h2" variant="h3" color="textPrimary">
                                ${planPrice}
                            </Typography>
                            <Typography component="h6" variant="h6" color="textSecondary">
                                /{planDuration}
                            </Typography>
                        </div>
                        <ul className="card__plan">
                            {planInfo.map((item, index) => 
                                 <Typography key={`${item}-${index}`} component="li" variant="subtitle1">
                                    {item}
                                </Typography>  
                            )}
                        </ul>
                    </CardContent>
                    <CardActions>
                        <Button variant="outlined" fullWidth={true}>
                            {btnLabel}
                        </Button>
                    </CardActions>
                </Card>
            </Paper>
        );
  }
}