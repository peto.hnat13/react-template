import React from "react";
import { Box } from "@material-ui/core";
import Skeleton from "@material-ui/lab/Skeleton";

export default function DetailPlaceholder() {
    return (
        <Box pt={4}>
            <Skeleton variant="text" width="50%" height={24} />
            <Skeleton variant="text" width="60%" height={24} />
            <Skeleton variant="text" width="45%" />
            <Skeleton variant="text" width="60%" height={24} />
            <Skeleton variant="text" width="45%" />
            <Skeleton variant="text" width="60%" height={24} />
            <Skeleton variant="text" width="45%" />
        </Box>
    );
}
