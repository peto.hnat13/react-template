
import React from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { FORM_ERRORS } from "ui/forms/SignUpForm/ErrorMessages";

interface IProps {
    value: any;
    name: string;
    required: undefined | boolean,
    type: string;
    matchRegex?: string;
    isConfirmPassword?: boolean;
    [key: string]: any;
};

interface TextFieldValidatorRules {
    fieldValidators: any[] | string[];
    fieldErrorMessages: any[] | string[];
} 

// TODO Zatial sa to nepouziva, lebo to nemalo zmysel
export default class TextFieldValidator extends React.PureComponent<IProps> {
    private validationRules: TextFieldValidatorRules = {
        fieldValidators: [],
        fieldErrorMessages: [],
    };

    componentDidMount() {
        this.validationRules = this.getValidatorsAndErrorMessages();
        // custom rule will have name 'isPasswordMatch'
        if (this.props.type === "password") {
            ValidatorForm.addValidationRule("isPasswordMatch", (value) => value === this.props.value);
        }
    }

    public render() {
        const {value, name} = this.props;
        return (
            <TextValidator
                value={value}
                name={name}
                validators={this.validationRules.fieldValidators}
                errorMessages={this.validationRules.fieldErrorMessages}
                {...this.props}
            />
        );
    }

    getValidatorsAndErrorMessages(): TextFieldValidatorRules {   
        const rules: TextFieldValidatorRules = {
            fieldValidators: [],
            fieldErrorMessages: [],
        };
        
        if (this.props.required) {
            rules.fieldValidators.push("required");
            rules.fieldErrorMessages.push(FORM_ERRORS.DEFAULT.REQUIRED);
        }
        
        if (this.props.type === "email") {
            rules.fieldValidators.push("isEmail")
            rules.fieldErrorMessages.push(FORM_ERRORS.EMAIL_NOT_VALID);
        }

        return rules;
    }

    componentWillUnmount() {
        // remove rule when it is not needed
        if (this.props.type === "password") {
            ValidatorForm.removeValidationRule("isPasswordMatch");
        }
    }
}
