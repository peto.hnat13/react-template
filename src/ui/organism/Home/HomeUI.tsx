import { History } from "history";
import React from "react";
import CompanyUI from "ui/molecule/Company/CompanyUI";
import HowItWorksUI from "ui/molecule/HowItWorks/HowItWorksUI";
import PricingUI from "ui/molecule/Pricing/PricingUI";
import NavbarUI from "ui/molecule/Navbar/NavbarUI";
import { User } from "../User/state";

// Props received from selectors
export interface IDataProps {
	user: User;
}

// Props received from dispatchers
export interface ICallbackProps {
	onUserLogout(): void;
}

// External props passed from parent component
export interface IExternalProps {
  	history: History;
}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {}

export default class HomeUI extends React.PureComponent<Props, ILocalState> {

  	public render() {
		const {user, history, onUserLogout} = this.props;
		
		return (
			<React.Fragment>
				<NavbarUI user={user} history={history} onUserLogout={onUserLogout}/>
				<div>
					<CompanyUI/>
					<HowItWorksUI/>
					<PricingUI/>
				</div>
			</React.Fragment>
		);
  	}
}
