import {connect} from 'react-redux';
import dispatchers from "ui/organism/Home/dispatchers";
import selectors from "ui/organism/Home/selectors";
import HomeUI, {IDataProps, ICallbackProps, IExternalProps} from "ui/organism/Home/HomeUI";

export default connect<IDataProps, ICallbackProps, IExternalProps>(
    selectors,
    dispatchers,
)(
    HomeUI,
);
