export const NAMESPACE = "registerForm";

// Internal type definition
export interface IState {
    registrationDone: boolean;
    errors: null |any[];
}

export const DEFAULT_STATE: IState = {
    registrationDone: false,
    errors: null,
}
