import createReducer from "redux-create-fsa-reducer";
import Action from "ui/organism/Signup/action";
import userAction from "ui/organism/User/action";
import { Scream } from "ui/organism/BabySitter/BabySitterResource";
import { DEFAULT_STATE, IState } from "ui/organism/Signup/state";

export default createReducer(DEFAULT_STATE, {
    [userAction.SIGNUP_DONE](
        state: IState,
    ): IState {
        return {
            ...state,
            registrationDone: true,
        };
    },

    [Action.BANNER_CLOSE](
        state: IState,
    ): IState {
        return {
            ...state,
            registrationDone: false,
        };
    },
});
