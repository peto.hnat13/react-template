import Action from "ui/organism/Signup/action";
import userAction from "ui/organism/User/action";
import {ICallbackProps} from "./SignupUI";
import { RegisterCredentials } from "ui/organism/User/state";

export default {
    onUserSignup: (credentials: RegisterCredentials) => userAction.userSignup(credentials),
    onBannerClose: () => Action.bannerClose(),
} as ICallbackProps;
