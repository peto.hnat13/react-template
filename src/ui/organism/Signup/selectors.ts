import { createSelector, createStructuredSelector } from "reselect";
import { IDataProps, IExternalProps } from "ui/organism/Signup/SignupUI";
import { userStateSelector } from "ui/organism/User/selectors";
import { NAMESPACE, IState, DEFAULT_STATE } from "ui/organism/Signup/state";

export const signupStateSelector = (globalState: unknown): IState => (
    (globalState && (globalState as any))[NAMESPACE] || DEFAULT_STATE
);

export const isRegistrationDoneSelector = createSelector(
    signupStateSelector,
    (state: IState): boolean => state && state.registrationDone
);

export default createStructuredSelector<unknown, IExternalProps, IDataProps>({
    user: userStateSelector,
    isRegistrationDone: isRegistrationDoneSelector,
});
