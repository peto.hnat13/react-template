import { FluxStandardAction } from "flux-standard-action";

const Action = {
    BANNER_CLOSE: "signup.BANNER_CLOSE",

    bannerClose(): FluxStandardAction<string> {
        const action: any = {
            type: Action.BANNER_CLOSE,
        }; 
        
        return action as FluxStandardAction<string>;
    },

}

export default Action;
