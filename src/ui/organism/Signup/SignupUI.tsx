import {Box, Container, Grid, MenuItem, Paper, Select, TextField, Button} from "@material-ui/core";
import {History} from "history";
import React from "react";
import { ROUTE } from "app/routes";
import RouterLinkUI from "ui/atom/RouterLink/RouterLinkUI";
import SectionTitleUI from "ui/atom/SectionTitle/SectionTitleUI";
import NavbarUI from "ui/molecule/Navbar/NavbarUI";
import { User, RegisterCredentials } from "ui/organism/User/state";
import { APP_CONFIG } from "app/config";
import SignupFormUI from "ui/forms/SignUpForm/SignUpFormUI";
import { Banner } from "material-ui-banner";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";

// Props received from selectors
export interface IDataProps {
    user: User;
    isRegistrationDone: boolean;
}

// Props received from dispatchers
export interface ICallbackProps {
    onUserSignup(credentials: RegisterCredentials): void;
    onBannerClose(): void;
}

// External props passed from parent component
export interface IExternalProps {
	history: History;
}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {
    isBannerOpen: boolean;
}

const REDIRECT_LABEL = "Already have an account?";

export default class SignupUI extends React.PureComponent<Props, ILocalState> {
    public state = {
        isBannerOpen: true,
    }

	public componentDidUpdate(prevProps: Props) {
		const {user, history} = this.props;

		if (user && user.isLogged) {
			history.push(ROUTE.DASHBOARD);
        }
        
        if (prevProps.isRegistrationDone !== this.props.isRegistrationDone) {
            this.setState({
                isBannerOpen: true,
            });
        }
	}

  	public render() {
		const {user, history, isRegistrationDone} = this.props;

		return (
			<>
				<NavbarUI user={user} history={history}/>
                <Container component="div" maxWidth="sm">
                    {isRegistrationDone ? 
                        <Banner
                            icon={<CheckCircleIcon/>}
                            label={APP_CONFIG.FORM.REGISTRATION.SUCCESS_MESSAGE}
                            showDismissButton
                            buttonLabel="Poslať znova"
                            buttonProps={{
                                color: "secondary",
                            }}
                            onClose={this.onBannerClose}
                            buttonOnClick={this.onResendEmail}
                            open={this.state.isBannerOpen}
                        />
                    :
                        <Box component="div" className="form__box">
                            <Paper elevation={0} className="form__paper form__paper--color">
                                <SectionTitleUI label="Sign Up"/>
                                <RouterLinkUI to={ROUTE.LOGIN} label={REDIRECT_LABEL}/>
                                <SignupFormUI onUserSignup={this.props.onUserSignup} />
                            </Paper>
                        </Box>
                    }
                </Container>
			</>
		);
    }

    public componentWillUnmount() {
        if (this.state.isBannerOpen) {
            this.props.onBannerClose();
        }
    }

    private onBannerClose = () => {
        this.setState({
            isBannerOpen: false,
        });

        // TODO clear state after dismiss
        this.props.history.push(ROUTE.LOGIN);
    }

    private onResendEmail = () => {
        // TODO zavolat endpoint na znovuposlanie emailu o uspesnej registracii
        console.log("Resend email");
    }
}
