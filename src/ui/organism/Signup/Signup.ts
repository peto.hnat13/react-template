import {connect} from 'react-redux';
import dispatchers from "ui/organism/Signup/dispatchers";
import selectors from "ui/organism/Signup/selectors";
import SignupUI, {IDataProps ,ICallbackProps, IExternalProps} from "ui/organism/Signup/SignupUI";

export default connect<IDataProps, ICallbackProps, IExternalProps>(
    selectors,
    dispatchers,
)(
    SignupUI,
);
