import {createSelector} from "reselect";
import {NAMESPACE, IState, DEFAULT_STATE} from "ui/organism/User/state";
import { UserRoleType } from "ui/organism/RegisteredUsers/RegisteredUsersResource";

// Used in clientRenderer
export const userStateSelector = (globalState: unknown): IState => (
    (globalState && (globalState as any))[NAMESPACE] || DEFAULT_STATE
);

export const isUserSignedInSelector = createSelector(
    userStateSelector,
    (state: IState): boolean => state && state.isLogged
);

export const userRoleSelector = createSelector(
    userStateSelector,
    (state: IState): null | UserRoleType => state && state.role
);
