import {ErrorFluxStandardAction, FluxStandardAction} from 'flux-standard-action';
import {User, UserCredentials, RegisterCredentials} from './state';

export type loginActionType = FluxStandardAction<string, UserCredentials>;

const Action = {
    LOGIN: "user.LOGIN",
    LOGOUT: "user.LOGOUT",
    RESET: "user.RESET",
    REFRESH_TOKEN: "user.REFRESH_TOKEN",
    FETCH_USER_DETAIL: "user.FETCH_USER_DETAIL",
    DETAIL_FETCH_DONE: "user.DETAIL_FETCH_DONE",
    DETAIL_FETCH_FAILED: "user.DETAIL_FETCH_FAILED",
    SIGNUP: "user.SIGNUP",
    SIGNUP_DONE: "user.SIGNUP_DONE",
    SIGNUP_FAILED: "user.SIGNUP_FAILED",

    userLogin(
        payload: {email: string, password: string},
    ): loginActionType {
        const action: any = {
            type: Action.LOGIN,
            payload: {
                email: payload.email,
                password: payload.password,
            },
        }; 
        
        return action as loginActionType;
    },    

    userLogout(): FluxStandardAction<string>  {
        const action: any = {
            type: Action.LOGOUT,
        };
        
        return action as FluxStandardAction<string>;
    },

    userSignup(
        payload: RegisterCredentials
    ): FluxStandardAction<string, RegisterCredentials> {
            const action: any = {
            type: Action.SIGNUP,
            payload,
        };
        return action as FluxStandardAction<string, RegisterCredentials>;
    },

    userSignupDone(): FluxStandardAction<string> {
        const action: any = {
            type: Action.SIGNUP_DONE,
        };
        return action as FluxStandardAction<string>;
    },

    reset(): FluxStandardAction<string>  {
        const action: any = {
            type: Action.RESET,
        };
        
        return action as FluxStandardAction<string>;
    },

    // TODO User Logout
    fetchUserDetail(
        payload: string,
    ): FluxStandardAction<string, string> {
        const action: any = {
            type: Action.DETAIL_FETCH_DONE,
            payload,
        }; 
        
        return action as FluxStandardAction<string, string>;
    },

    userDetailFetchDone(
        payload: {user: User},
    ): FluxStandardAction<string, {user: User}> {
        const action: any = {
            type: Action.DETAIL_FETCH_DONE,
            payload: payload.user,
        }; 
        
        return action as FluxStandardAction<string, {user: User}>;
    },

    userDetailFetchFailed(
        payload: Error,
    ): ErrorFluxStandardAction<string, Error> {
        const action: any = {
            type: Action.DETAIL_FETCH_FAILED,
            payload: payload,
            error: true,
        };

        return action as ErrorFluxStandardAction<string, Error>;
    },
};

export default Action;