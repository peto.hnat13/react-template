import { call, put } from "redux-saga/effects";
import UserAction, {loginActionType} from "./action";
import Cookies from 'universal-cookie';
import { login, getUserDetail, signup } from "app/AppClient";
import { createWatchDaemon } from "sagas/sagaHelpers";
import {User, UserCredentials, RegisterCredentials} from "./state";

const VERIFICATION_TOKEN_COOKIE = "verificationToken";
const VERIFICATION_TOKEN_COOKIE_EXPIRATION = 1000 * 60 * 60;

function* loginUser(loginAction: loginActionType) {
	try {
        console.log("Trying to get verification token");
        const cookies = new Cookies();
        const verificationToken: null | string = yield call(
            getVerificationToken,
            loginAction.payload as UserCredentials
        );

		if (!verificationToken) {
			return;
		}

        cookies.set(VERIFICATION_TOKEN_COOKIE, verificationToken, {
            expires: new Date(new Date().getTime() + VERIFICATION_TOKEN_COOKIE_EXPIRATION)
        });
        yield call(fetchUserDetail, verificationToken);

	} catch(apiError) {
        console.error(apiError);
        // yield put(DashboardAction.screamDataFetchFailed(apiError as Error));
	}
}

function* signupUser({payload: credentials}: {payload: RegisterCredentials}) {
    try {
        yield call(signup, credentials);
        yield put(UserAction.userSignupDone());
    } catch (apiError) {
        console.error(apiError);
    }
}

export function* autologinUser() {
    const isTokenValid = yield call(verifyToken);
    console.log("isTokenValid", isTokenValid);
}

function* verifyToken() {
    try {
        const cookies = new Cookies();
        const verificationToken = cookies.get(VERIFICATION_TOKEN_COOKIE);
        
        if (!verificationToken) {
            return false;
        }

        const user: User = yield call(fetchUserDetail, verificationToken);
        if (!user) {
            return false;
        }
        
        return true;
    } catch(apiError) {
        console.error(apiError);
    }
}

export function getVerificationTokenFromCookies() {
    const cookies = new Cookies();
    return cookies.get("verificationToken");
}

export function* getVerificationToken(credentials: UserCredentials) {
    try {
        const cookies = new Cookies();
        const verificationToken = cookies.get("verificationToken");

        if (!verificationToken) {
            return yield call(login, credentials);
        }

        return verificationToken;
    } catch(apiError) {
        console.error(apiError);
    }
}

function* fetchUserDetail(token: string) {
    try {
        const userWithDetail: User = yield call(getUserDetail, token);
        yield put(UserAction.userDetailFetchDone({
            user: userWithDetail,
        }));
    } catch(apiError) {
        console.error(apiError);
        // yield put(DashboardAction.screamDataFetchFailed(apiError as Error));
	}
}

function* logoutUser() {
    const cookies = new Cookies();
    if (cookies.get(VERIFICATION_TOKEN_COOKIE)) {
        cookies.remove(VERIFICATION_TOKEN_COOKIE);
    }

    yield put(UserAction.reset());
}

// Running at server
export const init: Array<() => any> = [];

export const daemons: Array<() => any> = [
    createWatchDaemon(UserAction.LOGIN, loginUser),
    createWatchDaemon(UserAction.LOGOUT, logoutUser),
    createWatchDaemon(UserAction.SIGNUP, signupUser),
    createWatchDaemon(UserAction.FETCH_USER_DETAIL, fetchUserDetail),
];
