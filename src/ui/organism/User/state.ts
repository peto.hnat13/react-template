import {NotificationResponse} from "ui/organism/Dashboard/state";
import { UserRoleType } from "../RegisteredUsers/RegisteredUsersResource";

export const NAMESPACE = "user";

export interface UserResponse {
    credentials: {
        bio: string;
        createdAt: string;
        email: string;
        imageUrl: null | string;
        premiumSearchCount: number;
        role: UserRoleType;
        userId: string;
    }
    likes: any[];
    notifications: NotificationResponse[];
}

export interface User {
    bio: null | string;
    createdAt: string;
    email: string;
    imageUrl: null | string;
    userId: string;
    notifications: null | NotificationResponse[];
    isLogged: boolean;
    premiumSearchCount: number;
    role: UserRoleType | null;
}

export interface UserCredentials {
    email: string;
    password: string;
}

export interface RegisterCredentials {
    email: string;
    password: string;
    confirmPassword: string;
    // phone: null | string;
}

export type IState = User;

export const DEFAULT_STATE: IState = {
    bio: "",
    createdAt: "",
    email: "",
    imageUrl: null,
    userId:"",
    notifications: null,
    isLogged: false,
    role: null,
    premiumSearchCount: 0,
};
