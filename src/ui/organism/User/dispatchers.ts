import userAction from "ui/organism/User/action";

export default {
    onUserLogin: (email: string, password: string) => userAction.userLogin({
        email,
        password,
    }),
};
