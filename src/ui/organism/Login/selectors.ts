import {createStructuredSelector} from 'reselect';
import {IDataProps, IExternalProps} from "ui/organism/Login/LoginUI";
import {userStateSelector} from "ui/organism/User/selectors";

export default createStructuredSelector<unknown, IExternalProps, IDataProps>({
    user: userStateSelector,
});
