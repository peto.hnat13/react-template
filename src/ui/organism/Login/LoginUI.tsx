import React, { FormEvent, ChangeEvent } from "react";
import { Box, Container, Paper } from "@material-ui/core";
import SectionTitleUI from "ui/atom/SectionTitle/SectionTitleUI";
import RouterLinkUI from "ui/atom/RouterLink/RouterLinkUI";
import { User } from "ui/organism/User/state";
import {History} from "history";
import {ROUTE} from "app/routes";
import NavbarUI from "ui/molecule/Navbar/NavbarUI";
import LoginFormUI from "ui/forms/LoginForm/LoginFormUI";

// Props received from selectors
export interface IDataProps {
    user: User;
}

// Props received from dispatchers
export interface ICallbackProps {
	onUserLogin(email: string, password: string): void; 
}

// External props passed from parent component
export interface IExternalProps {
	history: History
}

type Props = IDataProps & ICallbackProps & IExternalProps;

const SIGNUP_LABEL = "Don't have an account? Sign Up";

export default class LoginUI extends React.PureComponent<Props> {
	public componentDidUpdate(prevProps: Props) {
		const {user, history} = this.props;
		const logingNow = !prevProps.user && this.props.user && this.props.user.isLogged;
		const isLogged = this.props.user && this.props.user.isLogged;
		const loggedNavigatingToLoginPage = isLogged && history.location.pathname === ROUTE.LOGIN;

		//redirect na dashborad ked som prisiel z loginu
		if (logingNow || loggedNavigatingToLoginPage) {
			history.push(ROUTE.DASHBOARD); // ak sme sa prihlasili zobrazime dashboard
		}

		// Ak uz sme prihlaseny tak to odkade sme prisli
		if (user && user.isLogged && history.location.state && history.location.state.from) {
			history.push(history.location.state.from); // tu mame ulozene z redirect komponenty odkial sme prisli, a ked je prihlaseny tak ho tam znovu posleme 
		}
	}

  	public render() {
		const {user, history} = this.props;

		return (
			<React.Fragment>
				<NavbarUI user={user} history={history}/>
				<Container component="div" maxWidth="sm">
					<Box component="div" className="form__box">
						<Paper elevation={0} className="form__paper form__paper--color">
							<SectionTitleUI label="Login"/>
							<RouterLinkUI to={ROUTE.SIGNUP} label={SIGNUP_LABEL}/>
							<LoginFormUI onUserLogin={this.props.onUserLogin}/>
						</Paper>
					</Box>
				</Container>
			</React.Fragment>
		);
	}
}
