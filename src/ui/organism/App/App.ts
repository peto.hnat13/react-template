import {connect} from 'react-redux';
import selectors from './selectors';
import AppUI, {ICallbackProps, IDataProps, IExternalProps} from './AppUI';

export default connect<IDataProps, ICallbackProps, IExternalProps>(
    selectors,
)(
    AppUI,
);
