import classnames from "classnames";
import React from "react";
import {Provider} from "react-redux";
import {Router, Route, Switch, Link, Redirect} from "react-router-dom";
import {Store} from "redux";
import styled from "styled-components";
import {APP_CONFIG} from "app/config";
import image1 from "images/image1.png";
import {routes, protectedRoutes, connectedComponentRouteConfig} from "app/routes";
import {RouteConfig} from "react-router-config";
import NavbarUI from "ui/molecule/Navbar/NavbarUI";
// import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import {theme} from "app/theme";
import FooterUI from "ui/molecule/Footer/FooterUI";
import PrivateRoute from "ui/molecule/PrivateRoute/PrivateRouteUI";
import {History} from "history";
import {User} from "../User/state";
import DashboardUI from "ui/organism/Dashboard/Dashboard";

// Props received from selectors
export interface IDataProps {
    applicationVersion: string;
    user: User;
}

// Props received from dispatchers
export interface ICallbackProps {
}

// External props passed from parent component
export interface IExternalProps {
    store: Store;
    history: History;
}

type Props = IDataProps & ICallbackProps & IExternalProps;

interface ILocalState {
}

export default class AppUI extends React.PureComponent<Props, ILocalState> {
    public render() {
        const {store, history, applicationVersion, user} = this.props;
        // const isDark = true;
        // const testImage = `${APP_CONFIG.COMMON.IMAGES.PATH}/image1.png`;
        // console.log("testImage Path", testImage);

        return (
            <MuiThemeProvider theme={theme}>
                <Provider store={store}>
                    <Router history={history}>
                        {/* <AppVersionStyledContainer className={classnames(
                        {
                            "isDark": isDark,
                        },
                    )}>
                        {applicationVersion}
                    </AppVersionStyledContainer>
                    <img src={image1} alt="Smiley face" height="42" width="42"/>*/}
                        <AppContainer>
                            <Switch>
                                {routes.map((route: connectedComponentRouteConfig, index: number) => (
                                    <Route
                                        key={index}
                                        exact={route.exact}
                                        path={route.path}
                                        component={route.component}
                                    />
                                ))}

                                <Route
                                    path="/dashboard"
                                    render={({ match: { path } }) => (
                                    <>
                                        <PrivateRoute path={`${path}/`} component={DashboardUI} user={user} />
                                        {/* <Route path={`${url}/home`} component={Dashboard} />
                                        <Route path={`${url}/users`} component={UserPage} /> */}
                                    </>
                                    )}
                                />
                                {/* {protectedRoutes.map((route: RouteConfig, index: number) => (
                                    <PrivateRoute
                                        key={`protectedRoute-${index}`}
                                        exact={route.exact}
                                        path={route.path}
                                        component={route.component}
                                        user={user}
                                    />
                                ))} */}
                            </Switch>
                        </AppContainer>
                        <FooterUI/>
                    </Router>
                </Provider>
            </MuiThemeProvider>
        );
    }
}

// Example how to used styled components
const AppContainer = styled.main`
  margin: 80px auto 0 auto;
  /* max-width: 1200px; */
`;
