export const NAMESPACE = "application";

export interface ICookie {
    [cookieName: string]: string;
}

export interface IState {
    // TODO add appstate
    // cookies: ICookie,
    appVersion: string;
}

export const DEFAULT_STATE: IState = {
    // cookies: {
    //     "testCookie": "123456", 
    // }
    appVersion: "1.0",
};
