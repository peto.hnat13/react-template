import createReducer from 'redux-create-fsa-reducer';
import Action from './action';
import { DEFAULT_STATE, IState } from './state';

export default createReducer(DEFAULT_STATE, {
    [Action.INIT_APP_VERSION](
        state: IState,
        version: string,
    ): IState {
        return {
            ...state,
            appVersion: version,
        };
    },
});