import { call, all, fork } from 'redux-saga/effects';
import { autologinUser } from "ui/organism/User/sagas";

function* rootSaga() {
    yield all([
		fork(loadInitDataAtClient),
    ]);
}

// TODO move to Dashboard Sagas and save to Redux
function* loadInitDataAtClient() {
	try {
		// TODO get verification token and verify it
		yield call(autologinUser);
	} catch(apiError) {
		console.error(apiError);
	}
}

// Running at server
export const init: Array<() => any> = [];
// Running at client
export const daemons: Array<() => any> = [
    rootSaga,
];
