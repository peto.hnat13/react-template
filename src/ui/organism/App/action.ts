import {FluxStandardAction} from "flux-standard-action";

const Action = {
    INIT_APP_VERSION: "application.INIT_APP_VERSION",

    initAppVersion(version: string): FluxStandardAction<string, string> {
        return {
            payload: version,
            type: Action.INIT_APP_VERSION,
        };
    },
}

export default Action;