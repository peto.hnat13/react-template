// export type UserRoleType = "admin" | "registered" | "guest";
export enum UserRoleType {
    ADMIN = "admin",
    REGISTERED = "registered",
    GUEST = "guest",
}

interface RegisterUserResponse {
    userId: string;
    email: string;
    createdAt: string;
    role: UserRoleType;
}

export interface RegisterUser {
    userId: string;
    email: string;
    createdAt: string;
    role: UserRoleType,
}

// TODO do register usera pridat role, defaultne sa dava guest pri registracii, admin je natvrdo v DB a pri potvrdeni sa to prepise na registered
export function createRegisteredUser(data: RegisterUserResponse): RegisterUser {
    const registeredUser: RegisterUser = {
        userId: data.userId,
        email: data.email,
        createdAt: data.createdAt,
        role: data.role,
    };

    return registeredUser;
}
