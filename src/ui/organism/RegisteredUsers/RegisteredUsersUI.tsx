import React from "react";
import { User } from "ui/organism/User/state";
import MUIDataTable, { MUIDataTableOptions, MUIDataTableColumnDef } from "mui-datatables";
import { RegisterUser, UserRoleType } from "./RegisteredUsersResource";
import { Select, MenuItem, Button, FormControl } from "@material-ui/core";
import SaveIcon from '@material-ui/icons/Save';

// Props received from selectors
export interface IDataProps {
    user: User;
    registeredUsers: null | RegisterUser[];
}

// Props received from dispatchers
export interface ICallbackProps {
    onUpdateUserRole(updatedUsers: SelectedRowWithRole[]): void;
    onFetchRegisteredUsers(): void;
}

// External props passed from parent component
export interface IExternalProps {}

type Props = IDataProps & ICallbackProps & IExternalProps;

export interface SelectedRowWithRole {
    id: string;
    role: UserRoleType;
}

interface ILocalState {
    selectedRoles: [] | SelectedRowWithRole[];
    registeredUsers:  null | RegisterUser[];
}

const TABLE_TITLE = "Registered Users";
const TABLE_OPTIONS: MUIDataTableOptions = {
    // filterType: "multiselect",
    responsive: "stacked",
    download: false,
    print: false,
};

export default class RegisteredUsersUI extends React.PureComponent<Props, ILocalState> {
    public state = {
        selectedRoles: [],
        registeredUsers: null,
    };
    private columNames: MUIDataTableColumnDef[] = this.createColumns();

    public componentDidMount() {
        this.props.onFetchRegisteredUsers();
    }

    public componentDidUpdate(prevProps: Props) {
        if (prevProps.registeredUsers !== this.props.registeredUsers) {
            this.setState({
                registeredUsers: this.props.registeredUsers,
            });
        }
    }

    public render() {
        const {user} = this.props;

        return (
            <div>
                <MUIDataTable
                    title={TABLE_TITLE}
                    data={this.state.registeredUsers || []}
                    columns={this.columNames}
                    options={{
                        ...TABLE_OPTIONS,
                    }}
                />
                {this.state.selectedRoles.length > 0 &&
                     <Button
                        variant="contained"
                        color="primary"
                        size="large"
                        type="submit"
                        onClick={this.onSubmit}
                        // className={classes.button}
                    >
                        <SaveIcon />Save Changes
                    </Button>
                }
            </div>
        );
    }

    public createColumns(): MUIDataTableColumnDef[] {
        return [
            {
                name: "userId", // Musi sediet s klucom v objekte
                label: "Id" // zastupne meno stlpca, ktore sa zobrazuje v tabulke
            },
            {
                name: "email",
                label: "Email",
            },
            {
                name: "createdAt",
                label: "Created"
            },
            {
                name: "role",
                label: "User Role",
                options: {
                    filter: false,
                    sort: false,
                    empty: true,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        // value sa berie z meta, tym mame aktualnu hodnotu co mame ulozenu v tabulke
                        // MenuItem value musi byt zhodne s tym z meta a tym sa select nastavi spravne podla dat v tabulke
                        const isAdmin = value === "admin";
                        return (
                            <FormControl disabled={isAdmin}>
                                 <Select
                                    value={value}
                                    onChange={(event: any) => {
                                        updateValue(event.target.value, null, null); // Update UI
                                        this.roleChange(event.target.value, tableMeta.rowData, tableMeta.rowIndex); // Save changes to local state
                                    }}
                                    inputProps={{
                                        name: "Role",
                                        id: "role",
                                    }}
                                >
                                    {isAdmin &&
                                        <MenuItem value="admin">Admin</MenuItem>
                                    }
                                    <MenuItem value="guest">Guest</MenuItem>
                                    <MenuItem value="registered">Registered</MenuItem>
                                </Select>
                            </FormControl>
                        );
                    }
                },
            },
        ];
    }

    public roleChange = (selectedRole: UserRoleType, rowData: string[], rowIndex: number) => {
        const userId = rowData[0];
        const actualSelectedRoles: SelectedRowWithRole[] = [...this.state.selectedRoles]; // kopia hodnoty, nie odkazu
        const newUserRole = { id: userId, role: selectedRole};
        const itemIndex = this.state.selectedRoles.findIndex((item: SelectedRowWithRole) => item.id === userId);

        if (itemIndex < 0) {
            // Ak zme to zmenili na hodnotu ktora tam uz bola, tak neupdatujeme stav ale neni to ulozene v stave
            if (this.props.registeredUsers![rowIndex].role === selectedRole) {
                return;
            }

            actualSelectedRoles.push(newUserRole);
        } else {
            // Ak zme to zmenili spat na hodnotu ktora tam uz bola, ale je to uz ulozene v stave na update, zmazeme to zo stavu
            if (this.props.registeredUsers![rowIndex].role === selectedRole) {
                actualSelectedRoles.splice(itemIndex, 1);
            } else {
                actualSelectedRoles[itemIndex] = newUserRole;
            }
        }

        this.setState({
            selectedRoles: actualSelectedRoles,
        });
    }

    onSubmit = (event: React.MouseEvent<HTMLElement>) => {
        this.props.onUpdateUserRole(this.state.selectedRoles);

        // Hide Button from UI
        this.setState({
            selectedRoles: [],
        });
    }
}
