import {ErrorFluxStandardAction, FluxStandardAction} from "flux-standard-action";
import { RegisterUser } from "ui/organism/RegisteredUsers/RegisteredUsersResource";
import { SelectedRowWithRole } from "ui/organism/RegisteredUsers/RegisteredUsersUI";

const Action = {
    USERS_DATA_FETCH: "registeredUsers.USERS_DATA_FETCH",
    USERS_DATA_FETCH_DONE: "registeredUsers.USERS_DATA_FETCH_DONE",
    USERS_DATA_FETCH_FAILED: "registeredUsers.USERS_DATA_FETCH_FAILED",
    USERS_ROLE_UPDATE: "registeredUsers.USERS_ROLE_UPDATE",
    USERS_ROLE_UPDATE_DONE: "registeredUsers.USERS_ROLE_UPDATE_DONE",
    USERS_ROLE_UPDATE_FAILED: "registeredUsers.USERS_ROLE_UPDATE_FAILED",
    
    registeredUsersDataFetch(): FluxStandardAction<string> {
        const action: any = {
            type: Action.USERS_DATA_FETCH,
            payload: null,
        }; 
        
        return action as FluxStandardAction<string>;
    },

    registeredUsersDataFetchDone(
        payload: {users: RegisterUser[]},
    ): FluxStandardAction<string, {users: RegisterUser[]}> {
        const action: any = {
            type: Action.USERS_DATA_FETCH_DONE,
            payload: payload.users,
        }; 
        
        return action as FluxStandardAction<string, {users: RegisterUser[]}>;
    },

    registeredUsersDataFetchFailed(
        payload: Error,
    ): ErrorFluxStandardAction<string, Error> {
        const action: any = {
            type: Action.USERS_DATA_FETCH_FAILED,
            payload: payload,
            error: true,
        } 

        return action as ErrorFluxStandardAction<string, Error>;
    },

    registeredUsersRoleUpdate(
        payload: {usersToUpdate: SelectedRowWithRole[]},
    ): FluxStandardAction<string, {usersToUpdate: SelectedRowWithRole[]}> {
        const action: any = {
            type: Action.USERS_ROLE_UPDATE,
            payload: payload.usersToUpdate,
        }; 
        
        return action as FluxStandardAction<string, {usersToUpdate: SelectedRowWithRole[]}>;
    },

    registeredUsersRoleUpdateDone(
        payload: {updatedUser: SelectedRowWithRole},
    ): FluxStandardAction<string, {updatedUser: SelectedRowWithRole}> {
        const action: any = {
            type: Action.USERS_ROLE_UPDATE_DONE,
            payload: payload.updatedUser,
        }; 
        
        return action as FluxStandardAction<string, {updatedUser: SelectedRowWithRole}>;
    },

    registeredUsersRoleUpdateFailed(
        payload: Error,
    ): ErrorFluxStandardAction<string, Error> {
        const action: any = {
            type: Action.USERS_ROLE_UPDATE_FAILED,
            payload: payload,
            error: true,
        } 

        return action as ErrorFluxStandardAction<string, Error>;
    },
}

export default Action;
