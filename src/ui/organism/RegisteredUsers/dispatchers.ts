import registeredUsersAction from "ui/organism/RegisteredUsers/action";
import { SelectedRowWithRole } from "ui/organism//RegisteredUsers/RegisteredUsersUI";

export default {
    onUpdateUserRole: (
        usersToUpdate: SelectedRowWithRole[]
    ) => registeredUsersAction.registeredUsersRoleUpdate({ usersToUpdate }),
    onFetchRegisteredUsers: registeredUsersAction.registeredUsersDataFetch,
};
