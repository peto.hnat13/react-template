import { createStructuredSelector } from 'reselect';
import { IDataProps, IExternalProps } from "ui/organism/RegisteredUsers/RegisteredUsersUI";
import { registeredUsersSelector } from "ui/organism/Dashboard/selectors";
import { userStateSelector } from "ui/organism/User/selectors";

export default createStructuredSelector<unknown, IExternalProps, IDataProps>({
    user: userStateSelector,
    registeredUsers: registeredUsersSelector,
});
