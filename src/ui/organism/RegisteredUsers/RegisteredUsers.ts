import {connect} from "react-redux";
import dispatchers from "ui/organism/RegisteredUsers/dispatchers";
import selectors from "ui/organism/RegisteredUsers/selectors";
import RegisteredUsersUI, {IDataProps, ICallbackProps, IExternalProps} from "ui/organism/RegisteredUsers/RegisteredUsersUI";

export default connect<IDataProps, ICallbackProps, IExternalProps>(
    selectors,
    dispatchers,
)(
    RegisteredUsersUI,
);
