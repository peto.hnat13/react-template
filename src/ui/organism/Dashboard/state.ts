import { RegisterUser } from "ui/organism/RegisteredUsers/RegisteredUsersResource";
import { Scream, GISABabysitterResponse, BabysitterSearchResult } from "ui/organism/BabySitter/BabySitterResource";

export const NAMESPACE = "dashboard";

enum NotificationType {
    LIKE,
    COMMENT,
}

// API response type definition

export interface NotificationResponse {
    createdAt: Date;
    read: boolean;
    recipient: string;
    screamId: string;
    sender: string;
    type: string;
}

export interface LikeResponse {
    screamId: string;
    userHandle: string;
}

export interface CommentResponse {
    body: string;
    createdAt: Date;
    screamId: string;
    userHandle: string;
    userImage: null | string;
}

// Internal type definition
export interface IState {
    screams: null | {
        [businessId: string]: Scream;
    };
    lastError: null | string;
    isModalOpen: boolean;
    registeredUsers: null | RegisterUser[];
    searchResult: null | BabysitterSearchResult;
    isLoading: boolean;
    isDetailLoading: boolean;
}

export const DEFAULT_STATE: IState = {
    screams: null,
    lastError: null,
    isModalOpen: false,
    registeredUsers: null,
    searchResult: null,
    isLoading: false,
    isDetailLoading: false,
}
