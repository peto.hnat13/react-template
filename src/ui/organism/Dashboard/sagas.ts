import { call, all, fork, put, select, spawn } from "redux-saga/effects";
// import BabySitterAction, { addBabysitterPayload } from "ui/organism/BabySitter/action";
import RegisteredUsersAction from "ui/organism/RegisteredUsers/action";
import { 
	// addScream,
	// deleteScreams,
	// getScreams,
	getUsers, 
	// updateScream,
	// getGISABabysitterDetail,
	// getFirebaseBabysitterDetail
} from "app/AppClient";
import { createWatchDaemon } from "sagas/sagaHelpers";
// import { getVerificationTokenFromCookies } from "ui/organism/User/sagas";
// import { screamsSelector } from 'ui/organism/Dashboard/selectors';
// import { DeletedRow, getDataFromSelectedRows } from "utils/DataTable";
// import { Scream, BabysitterSearchResult, GISABabysitterResponse } from "ui/organism/BabySitter/BabySitterResource";
// import { SelectedRowWithRole } from "ui/organism/RegisteredUsers/RegisteredUsersUI";
// import { UserRoleType } from "ui/organism/RegisteredUsers/RegisteredUsersResource";

function* rootSaga() {
    yield all([
		fork(loadInitDataAtClient),
    ]);
}

function* loadInitDataAtClient() {}

// function* addNewScream({payload: data}: {payload: addBabysitterPayload}) {
// 	try {
// 		const token = yield call(getVerificationTokenFromCookies);
// 		if (!token) {
// 			return;
// 		}
//
// 		const screamAdded = yield call(addScream, data, token);
// 		if (screamAdded) {
// 			yield call(getActualScreams);
// 			yield put(BabySitterAction.modalVisibilityChange(false));
// 		}
// 	} catch (apiError) {
// 		console.error(apiError);
// 	}
// }

// function* updateExistingScream({payload: {screamId, data}}: {payload: {screamId: string, data: addBabysitterPayload}}) {
// 	try {
// 		const token = yield call(getVerificationTokenFromCookies);
// 		if (!token) {
// 			return;
// 		}
//
// 		const screamAdded = yield call(updateScream, screamId, data, token);
// 		if (screamAdded) {
// 			yield call(getActualScreams);
// 			yield put(BabySitterAction.modalVisibilityChange(false));
// 		}
// 	} catch (apiError) {
// 		console.error(apiError);
// 	}
// }

// function* getActualScreams() {
// 	try {
// 		const screams = yield call(getScreams);
//
// 		if (!screams) {
// 			return;
// 		}
//         yield put(BabySitterAction.screamDataFetchDone(screams));
// 	} catch (apiError) {
//         console.error(apiError);
//         yield put(BabySitterAction.screamDataFetchFailed(apiError as Error));
// 	}
// }

// function* deleteOneScream(itemId: string, token: string) {
// 	try {
// 		// TODO optimistic update
// 		yield call(deleteScreams, itemId, token);
// 	} catch (apiError) {
// 		// TODO error handling
// 		console.error(apiError);
// 	}
// }

// function* deleteSelectedScreams({payload: deletedRows}: {payload: DeletedRow}) {
// 	try {
// 		const token = yield call(getVerificationTokenFromCookies);
// 		if (!token) {
// 			return;
// 		}
//
// 		const screams = yield select(screamsSelector);
// 		if (!screams) {
// 			return;
// 		}
//
// 		const selectedScreams: Scream[] = getDataFromSelectedRows(deletedRows, screams);
// 		const screamIds = selectedScreams.map(scream => scream.screamId);
//
// 		for(const screamId of screamIds) {
// 			yield spawn(deleteOneScream, screamId, token);
// 		}
//
// 		// Teraz sa v reduxe updatuje vsetky ID naraz
// 		yield put(BabySitterAction.deleteScreamsDone(screamIds));
// 	} catch (apiError) {
// 		console.error(apiError);
// 		// TODO Error handling ak zlyhaju requesty na mazanie
// 	}
// }
//
function* getRegisteredUsers() {
	try {
		const registeredUsers = yield call(getUsers);

		if (!registeredUsers) {
			return;
		}

		yield put(RegisteredUsersAction.registeredUsersDataFetchDone(registeredUsers));
	} catch (apiError) {
        console.error(apiError);
		yield put(RegisteredUsersAction.registeredUsersDataFetchFailed(apiError as Error));
	}
}

// function* updateOneUserRole(userData: SelectedRowWithRole) {
// 	try {
// 		// Optimistic update, este pred volanim API
// 		yield put(RegisteredUsersAction.registeredUsersRoleUpdateDone({ updatedUser: userData }));
// 		// TODO call api to update
//
// 	} catch (apiError) {
// 		console.error(apiError);
// 		yield put(RegisteredUsersAction.registeredUsersRoleUpdateFailed(apiError as Error));
// 		// Revert Optimistic update for failed user request
// 		yield put(RegisteredUsersAction.registeredUsersRoleUpdateDone(
// 			{
// 				updatedUser: {
// 					id: userData.id,
// 					role: userData.role === UserRoleType.GUEST ? UserRoleType.REGISTERED : UserRoleType.GUEST,
// 				}
// 			}
// 		));
// 	}
// }

// Nieje to atomicka operacia, nevieme tu robit optimistic update preto len prevolava dalsiu fciu
// function* updateUsersRoles({payload: usersToUpdate}: {payload: SelectedRowWithRole[]}) {
// 	for (const user of usersToUpdate) {
// 		yield spawn(updateOneUserRole, user);
// 	}
// }

// function* fetchGISADetail({payload: businessId}: {payload: string}) {
// 	try {
// 		const data = yield call(getGISABabysitterDetail, businessId);
// 		// TODO ulozit obdrzane data
// 		if (!data) {
// 			// TODO zapiseme do lastError ze sa nepodarilo stiahnut vysledok
// 		}
//
// 		yield put(BabySitterAction.GISADetailFetchDone({businessId, data}));
// 	} catch (apiError) {
// 		console.error(apiError);
// 	}
// }

// function* fetchFirebaseDetail({payload: businessId}: {payload: string}) {
// 	try {
// 		const data = yield call(getFirebaseBabysitterDetail, businessId);
// 		// TODO ulozit obdrzane data
// 		if (!data) {
// 			// TODO zapiseme do lastError ze sa nepodarilo stiahnut vysledok
// 		}
// 		console.log("stiahol som toto", data);
// 		yield put(BabySitterAction.firebaseDetailFetchDone({businessId, data}));
// 	} catch (apiError) {
// 		console.error(apiError);
// 	}
// }

// function* searchPhrase({payload: businessId}: {payload: string}) {
// 	try {
// 		const data: GISABabysitterResponse[] = yield call(getGISABabysitterDetail, businessId);
// 		// TODO ulozit obdrzane data
// 		if (!data || !data.length) {
// 			// TODO zapiseme do lastError ze sa nepodarilo stiahnut vysledok
// 		}
//
// 		const basicData: BabysitterSearchResult = {
// 			businessId,
// 			detail: data[0],
// 		}
//
// 		yield put(BabySitterAction.searchDone({data: basicData}));
// 	} catch (apiError) {
// 		console.error(apiError);
// 		yield put(BabySitterAction.searchFailed(apiError));
// 	}
// }

// function* resetSearch() {
// 	yield put(BabySitterAction.searchReset());
// }

// Running at server
export const init: Array<() => any> = [];
// Running at client
export const daemons: Array<() => any> = [
	rootSaga,
	// createWatchDaemon(BabySitterAction.ADD_SCREAM, addNewScream),
	// createWatchDaemon(BabySitterAction.UPDATE_SCREAM, updateExistingScream),
	// createWatchDaemon(BabySitterAction.DELETE_SCREAMS, deleteSelectedScreams),
	// createWatchDaemon(BabySitterAction.GISA_DETAIL_FETCH, fetchGISADetail),
	// createWatchDaemon(BabySitterAction.FIREBASE_DETAIL_FETCH, fetchFirebaseDetail),
	// createWatchDaemon(RegisteredUsersAction.USERS_ROLE_UPDATE, updateUsersRoles),
	// createWatchDaemon(BabySitterAction.SEARCH, searchPhrase),
	// createWatchDaemon(BabySitterAction.SEARCH_CLEAN, resetSearch),
	// createWatchDaemon(BabySitterAction.SCREAM_DATA_FETCH, getActualScreams),
	createWatchDaemon(RegisteredUsersAction.USERS_DATA_FETCH, getRegisteredUsers),
];
