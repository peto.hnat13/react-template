import {UserResponse, User} from "ui/organism/User/state";

export function createUser(data: UserResponse): User {
    const loggedUser: User = {
        bio: data.credentials.bio || null,
        createdAt: data.credentials.createdAt,
        email: data.credentials.email,
        imageUrl: data.credentials.imageUrl || null,
        userId: data.credentials.userId,
        notifications: data.notifications || null, // TODO create Notification structure
        premiumSearchCount: data.credentials.premiumSearchCount || 0,
        isLogged: true,
        role: data.credentials.role,
    };
    
    return loggedUser;
}