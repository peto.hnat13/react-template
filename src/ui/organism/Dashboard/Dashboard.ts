import {connect} from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import dispatchers from "ui/organism/Dashboard/dispatchers";
import selectors from "ui/organism/Dashboard/selectors";
import DashboardUI, {customStyles, IDataProps, ICallbackProps, IExternalProps} from "ui/organism/Dashboard/DashboardUI";

export default withStyles(customStyles, { withTheme: true })(
    connect<IDataProps, ICallbackProps, IExternalProps>(
        selectors,
        dispatchers,
    )(
        DashboardUI,
    ),
);
