import { createSelector, createStructuredSelector } from "reselect";
import { NAMESPACE, IState, DEFAULT_STATE } from "./state";
import { userStateSelector } from "ui/organism/User/selectors";
import { IExternalProps, IDataProps } from "ui/organism/Dashboard/DashboardUI";
import { Scream, BabysitterSearchResult } from "ui/organism/BabySitter/BabySitterResource";
import { RegisterUser } from "ui/organism/RegisteredUsers/RegisteredUsersResource";

export const dashboardStateSelector = (globalState: unknown): IState => (
    (globalState && (globalState as any))[NAMESPACE] || DEFAULT_STATE
);

export const screamsSelector = createSelector(
    dashboardStateSelector,
    (dashboard: IState): null | {[businessId: string]: Scream} => dashboard && dashboard.screams
);

export const isModalOpenSelector = createSelector(
    dashboardStateSelector,
    (dashboard: IState): boolean => dashboard.isModalOpen
);

export const registeredUsersSelector = createSelector(
    dashboardStateSelector,
    (dashboard: IState): null | RegisterUser[] => dashboard && dashboard.registeredUsers
);

export const searchResultSelector = createSelector(
    dashboardStateSelector,
    (dashboard: IState): null | BabysitterSearchResult => dashboard && dashboard.searchResult
);

export const isLoadingSelector = createSelector(
    dashboardStateSelector,
    (dashboard: IState): boolean => dashboard && dashboard.isLoading
);

export const isDetailLoadingSelector = createSelector(
    dashboardStateSelector,
    (dashboard: IState): boolean => dashboard && dashboard.isLoading
);

export default createStructuredSelector<unknown, IExternalProps, IDataProps>({
    loggedUser: userStateSelector,
});
