import React from "react";
import { Switch, Route } from "react-router-dom";
import { Theme, createMuiTheme, createStyles } from "@material-ui/core/styles";
import { User } from "ui/organism/User/state";
import DashboardNavbarUI from "ui/molecule/DashboardNavbar/DashboardNavbar";
import PrivateRoute from "ui/molecule/PrivateRoute/PrivateRouteUI";
import { ROUTE } from "app/routes";

const theme = createMuiTheme();
export const customStyles = createStyles({
    root: {
        display: "flex",
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
});

// Props received from selectors
export interface IDataProps {
    loggedUser: User;
}

// Props received from dispatchers
export interface ICallbackProps {
    onUserLogout(): void;
}

// External props passed from parent component
export interface IExternalProps {}

interface CSSStyles {
    classes: {
        root: string;
        content: string;
    };
    theme: Theme; // TODO ak chceme pouzit temu v komponente
}

type Props = IDataProps & ICallbackProps & IExternalProps & CSSStyles;

interface ILocalState {
    mobileOpen: boolean;
};

export default class DashboardUI extends React.PureComponent<Props, ILocalState> {
    public state = {
        mobileOpen: false,
    };    

    public render() {
        const {loggedUser, classes} = this.props;
        console.log(this.props);

        // Routy radime od najspecifickejsich az po najobecnejsie, inak sa nam to nematchne
        return (
            <div className={classes.root}>
                <DashboardNavbarUI loggedUser={loggedUser} onUserLogout={this.props.onUserLogout}/>
                <main className={classes.content}>
                    <div>
                        Dashboard content, tu pridaj do switchu dalsie routy pre dalsie pod stranky, a v navigacii uprav linky,
                        aby sa prepla url a vyrendrovala príslušná komponenta
                    </div>

                    {/*<Switch>*/}
                        {/*<PrivateRoute*/}
                            {/*key="detail"*/}
                            {/*path="/dashboard/baby-sitters/:id"*/}
                            {/*component={BabysitterWithDetailUI}*/}
                            {/*user={loggedUser}*/}
                        {/*/>*/}
                    {/*</Switch>*/}
                </main>
            </div>
        );
    }

    public handleDrawerToggle = () => {
        this.setState({
            mobileOpen: !this.state.mobileOpen,
        });
    };
}
