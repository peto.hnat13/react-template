export const FORM_ERRORS = {
    DEFAULT: {
        REQUIRED: "This field is required",
    },
    EMAIL_NOT_VALID: "Please type valid email address",
    PASSWORD_NOT_MATCH: "Password not match",
    PHONE_ONLY_NUMBERS: "Phone must contain only numbers",
    PHONE_EXACT_LENGTH: "Phone number must have 9 characters",
    ADD_BABYSITTER: {
        BUSINESS_ID_LENGTH: "Business ID must have exact 8 numbers",
    }
};
