import { Grid, MenuItem, Select} from "@material-ui/core";
import React, {ChangeEvent, FormEvent} from "react";
import SubmitButtonUI from "ui/atom/SubmitButton/SubmitButtonUI";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { RegisterCredentials } from "ui/organism/User/state";
import TextFieldValidator from "ui/atom/TextFieldValidator/TextFieldValidator";
import { FORM_ERRORS } from "ui/forms/SignUpForm/ErrorMessages";

export interface IExternalProps {
    onUserSignup(credentials: RegisterCredentials): void;
}

type Props = IExternalProps;

interface ILocalState {
    email: string;
    password: string;
    confirmPassword: string;
    phone: string | number;
    errors: null | string[];
}

export default class SignupFormUI extends React.PureComponent<Props, ILocalState> {
    public state = {
        email: "",
        password: "",
        confirmPassword: "",
        phone: "",
        errors: null,
    };

    componentDidMount() {
        ValidatorForm.addValidationRule("isPasswordMatch", (value) => value === this.state.password);
        ValidatorForm.addValidationRule("isValidPhoneNumberLength", (value) => value.length === 9);
    }

  	public render() {
		return (
            <ValidatorForm noValidate onSubmit={this.onSubmit}>
                <TextValidator
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    onChange={this.handleChange}
                    value={this.state.email}
                    autoFocus
                    validators={["required", "isEmail"]}
                    errorMessages={[FORM_ERRORS.DEFAULT.REQUIRED, FORM_ERRORS.EMAIL_NOT_VALID]}
                />
                <TextValidator
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    onChange={this.handleChange}
                    value={this.state.password}
                    validators={["required"]}
                    errorMessages={[FORM_ERRORS.DEFAULT.REQUIRED]}
                />
                <TextValidator
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="confirmPassword"
                    label="Confirm Password"
                    type="password"
                    id="confirmPassword"
                    onChange={this.handleChange}
                    value={this.state.confirmPassword}
                    validators={["required", "isPasswordMatch"]}
                    errorMessages={[FORM_ERRORS.DEFAULT.REQUIRED, FORM_ERRORS.PASSWORD_NOT_MATCH]}
                />
                <SubmitButtonUI label="Sign Up"/>
                 {/* <TextFieldValidator  
                    fullWidth
                    margin="normal"
                    autoFocus
                    id="email"
                    name="email"
                    label="Email Address"
                    required
                    type="email"
                    value={this.state.email} 
                    variant="outlined"
                    onChange={this.onEmailChange}
                /> */}
                {/* <Grid container spacing={1} alignItems="flex-end">
                    <Grid item>
                        <Select
                            id="demo-simple-select"
                            value={10}
                            // value={age}
                            // onChange={handleChange}
                        >
                            <MenuItem value={10}>+421</MenuItem>
                            <MenuItem value={20}>+420</MenuItem>
                        </Select>
                    </Grid>
                    <Grid item>
                        <TextValidator
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            name="phone"
                            label="Phone"
                            type="text"
                            id="phone"
                            onChange={this.onPhoneChange}
                            value={this.state.phone}
                            validators={["matchRegexp:[0-9]$", "isValidPhoneNumberLength"]}
                            errorMessages={[FORM_ERRORS.PHONE_ONLY_NUMBERS, FORM_ERRORS.PHONE_EXACT_LENGTH]}
                        />
                    </Grid>
                </Grid> */}
            </ValidatorForm>
		);
    }  

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {target: {name, value}} = event;

        // @ts-ignore
        this.setState({
            [name]: value,
        });
    }

    onSubmit = (event: FormEvent<Element>) => {
        event.preventDefault();
        const {email, password, confirmPassword, phone} = this.state;
        this.props.onUserSignup({email, password, confirmPassword});
	};

    componentWillUnmount() {
        ValidatorForm.removeValidationRule("isPasswordMatch");
        ValidatorForm.removeValidationRule("isValidPhoneNumberLength");
    }
}
