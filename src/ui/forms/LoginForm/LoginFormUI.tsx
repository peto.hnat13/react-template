import React, {ChangeEvent, FormEvent} from "react";
import SubmitButtonUI from "ui/atom/SubmitButton/SubmitButtonUI";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { FORM_ERRORS } from "ui/forms/SignUpForm/ErrorMessages";

export interface IExternalProps {
    onUserLogin(email: string, password: string): void; 
}

type Props = IExternalProps;

interface ILocalState {
    email: string;
	password: string;
	loading: boolean;
	errors: null | string[];
}

export default class LoginFormUI extends React.PureComponent<Props, ILocalState> {
    public state = {
        email: "",
		password: "",
		loading: false,
		errors: null,
    };

  	public render() {
		return (
            <ValidatorForm noValidate onSubmit={this.onSubmit}>
                <TextValidator
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    onChange={this.handleChange}
                    value={this.state.email}
                    autoFocus
                    validators={["required", "isEmail"]}
                    errorMessages={[FORM_ERRORS.DEFAULT.REQUIRED, FORM_ERRORS.EMAIL_NOT_VALID]}
                />
                <TextValidator
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    onChange={this.handleChange}
                    value={this.state.password}
                    validators={["required"]}
                    errorMessages={[FORM_ERRORS.DEFAULT.REQUIRED]}
                />
                <SubmitButtonUI label="Login" loading={this.state.loading}/>
            </ValidatorForm>
		);
    }  

    handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const {target: {name, value}} = event;

        // @ts-ignore
        this.setState({
            [name]: value,
        });
    }

    onSubmit = (event: FormEvent<Element>) => {
		event.preventDefault();
		this.setState({
			loading: true,
		});

		if (this.state.email !== "" && this.state.password !== "") {
			this.props.onUserLogin(this.state.email, this.state.password);
		}
	};
}
