import React, {ChangeEvent, FormEvent} from "react";
import { connect } from "react-redux";
import { withStyles, createMuiTheme, createStyles, Theme } from "@material-ui/core/styles";
import { 
    Checkbox, 
    MenuItem, 
    Select, 
    Dialog, 
    AppBar, 
    Toolbar, 
    IconButton, 
    Typography, 
    Button, 
    Container, 
    Box, 
    Paper,
    Slide,
    FormGroup,
    FormControlLabel,
    FormControl,
    FormLabel,
    TextareaAutosize
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { FORM_ERRORS } from "ui/forms/SignUpForm/ErrorMessages";
import { TransitionProps } from "@material-ui/core/transitions";
import CloseIcon from "@material-ui/icons/Close";
import Rating, { IconContainerProps } from '@material-ui/lab/Rating';
import { addBabysitterPayload } from "ui/organism/BabySitter/action";
import { Scream } from "ui/organism/BabySitter/BabySitterResource";
import Snackbar from "ui/molecule/Snackbar/Snackbar";
import {theme} from "app/theme";

export const customStyles = createStyles({
    appBar: {
        position: "relative",
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    formControl: {
        //margin: theme.spacing(3),
        width: "100%",
    },
    container: {
        marginTop: theme.spacing(6),
    },
    textField: {
        marginLeft: "auto",
        marginRight: "auto",            
        width: "100%",
        maxWidth: "100%",
        height: "200px !important",
        maxHeight: "200px",
        paddingBottom: 0,
        marginTop: 0,
        fontWeight: 500,
    },
    textfieldLabel: {
        marginBottom: "9px",
    },
    input: {
        color: 'white'
    },
});
const Transition = React.forwardRef<unknown, TransitionProps>(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export interface IExternalProps {
    open: boolean;
    selectedBabysitter: null | Scream;
    onAddBabysitter(data: addBabysitterPayload): void;
    onUpdateBabysitter(screamId: string, data: addBabysitterPayload): void;
    getRating(ratingFactors: BabySitterRatingFactors): number;
    onCloseModal(): void;
}

interface CSSStyles {
    classes: {
        container: string;
        appBar: string;
        title: string;
        textField: string;
        textfieldLabel: string;
        formControl: string;
        input: string;
    };
    theme: Theme; // TODO ak chceme pouzit temu v komponente
}

type Props = IExternalProps & CSSStyles;

export interface BabySitterRatingFactors {
    workingPermission: boolean;
    alcohol: boolean;
    robbery: boolean;
    notes?: string; 
}

interface ILocalState {
    businessId: string;
    gender: string;
    workingPermission: boolean;
    alcohol: boolean;
    robbery: boolean;
    privacyConsent: boolean;
    notes: string;
    loading: boolean;
    errors: null | string[];
    isSnackbarOpen: boolean;
}

const DEFAULT_STATE = {
    businessId: "",
    gender: "male",
    workingPermission: false,
    alcohol: false,
    robbery: false,
    privacyConsent: false,
    notes: "",
    loading: false,
    errors: null,
    isSnackbarOpen: false,
};

class AddBabysitterFormUI extends React.PureComponent<Props, ILocalState> {
    public state = DEFAULT_STATE;

    public componentDidMount() {
        ValidatorForm.addValidationRule("isValidBusinessId", (value) => value.length === 8);
    }

    public componentDidUpdate(prevProps: Props) {
        // Ak mame stale otvoreny modal tak nic
        if (prevProps.open === this.props.open) {
            return;
        }

        const {selectedBabysitter} = this.props;
        if (selectedBabysitter) {
            this.setState({
                businessId: selectedBabysitter.businessId,
                gender: selectedBabysitter.gender,
                workingPermission: selectedBabysitter.workingPermission,
                alcohol: selectedBabysitter.alcohol,
                robbery: selectedBabysitter.robbery,
                privacyConsent: selectedBabysitter.privacyConsent,
                notes: selectedBabysitter.notes,
            });
        } else {
            this.setState(DEFAULT_STATE);
        }
    }

  	public render() {
        const {open, classes, getRating} = this.props;
        const {workingPermission, alcohol, robbery} = this.state;

		return (
            <div>
                <Dialog fullScreen open={open} onClose={this.onCloseModal} TransitionComponent={Transition}>
                    <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={this.onCloseModal} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Create New Baby Sitter
                        </Typography>
                        <Button color="inherit" onClick={this.onSubmit}>
                            save
                        </Button>
                    </Toolbar>
                    </AppBar>
                    <Container className={classes.container} component="div" maxWidth="sm">
                        <Box component="div" className="form__box">
                            <Paper elevation={0} className="form__paper form__paper--color">
                                <ValidatorForm noValidate onSubmit={this.onSubmit}>
                                    <Box component="div" mb={3} borderColor="transparent">
                                        <TextValidator
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            maxLength="8"
                                            id="businessId"
                                            label="Business ID"
                                            name="businessId"
                                            onChange={this.handleChange}
                                            value={this.state.businessId}
                                            autoFocus
                                            validators={["required", "isValidBusinessId"]}
                                            errorMessages={[FORM_ERRORS.DEFAULT.REQUIRED, FORM_ERRORS.ADD_BABYSITTER.BUSINESS_ID_LENGTH]}
                                        />
                                    </Box>
                                    <Box component="div" mb={3} borderColor="transparent">
                                        <FormControl component="fieldset" className={classes.formControl}>
                                            <FormLabel component="legend">Gender *</FormLabel>
                                            <FormGroup>
                                                <Select
                                                    required
                                                    name="gender"
                                                    id="gender"
                                                    value={this.state.gender}
                                                    onChange={this.handleChange}
                                                >
                                                    <MenuItem value="male">Male</MenuItem>
                                                    <MenuItem value="female">Female</MenuItem>
                                                </Select>
                                            </FormGroup>
                                        </FormControl>
                                    </Box>
                                    <Box component="div" mb={3} borderColor="transparent">
                                        <FormControl component="fieldset" className={classes.formControl}>
                                            <FormLabel component="legend">Rate Babysitter</FormLabel>
                                            <FormGroup>
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={this.state.workingPermission}
                                                            onChange={this.handlePermissionChange}
                                                            value={this.state.workingPermission}
                                                            color="primary"
                                                            inputProps={{
                                                                "aria-label": "working permission checkbox",
                                                            }}
                                                        />
                                                    }
                                                    label="Working premission"
                                                />
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={this.state.robbery}
                                                            onChange={this.handleRobbingChange}
                                                            value={this.state.robbery}
                                                            color="primary"
                                                            inputProps={{
                                                                "aria-label": "robbing checkbox",
                                                            }}
                                                        />
                                                    }
                                                    label="Problems with robbery"
                                                />
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={this.state.alcohol}
                                                            onChange={this.handleAlcoholChange}
                                                            value={this.state.alcohol}
                                                            color="primary"
                                                            inputProps={{
                                                                "aria-label": "alcohol checkbox",
                                                            }}
                                                        />
                                                    }
                                                    label="Problems with alcohol"
                                                />
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={this.state.privacyConsent}
                                                            onChange={this.handleConsentChange}
                                                            value={this.state.privacyConsent}
                                                            color="primary"
                                                            inputProps={{
                                                                "aria-label": "consent checkbox",
                                                            }}
                                                        />
                                                    }
                                                    label="Consent to the processing pof personal data *"
                                                />
                                            </FormGroup>
                                        </FormControl>
                                    </Box>
                                    <Box component="div" mb={3} borderColor="transparent">
                                        <FormControl component="fieldset" className={classes.formControl}>
                                            <FormLabel className={classes.textfieldLabel} component="legend">
                                                Additional Notes
                                            </FormLabel>
                                            <FormGroup>
                                                <TextareaAutosize
                                                    className={classes.textField}
                                                    rowsMax={8}
                                                    name="notes"
                                                    aria-label="maximum height"
                                                    placeholder="Optional Notes"
                                                    value={this.state.notes}
                                                    onChange={this.handleChange}
                                                />
                                            </FormGroup>
                                        </FormControl>
                                    </Box>
                                </ValidatorForm>
                                {getRating && 
                                    <Box component="div" mb={3} borderColor="transparent">
                                        <Typography component="legend">Computed Babysitter Rating</Typography>
                                        <Rating
                                            name="simple-controlled"
                                            value={getRating({workingPermission, alcohol, robbery})}
                                            readOnly={true}
                                        />
                                    </Box>
                                }
                            </Paper>
                        </Box>
                        {this.state.errors && this.state.errors[0] && this.state.isSnackbarOpen &&
                            <Snackbar 
                                message={this.state.errors && this.state.errors[0]} 
                                duration={6000} 
                                onSnacbarVisibilityChange={this.onSnacbarVisibilityChange} 
                                isOpen={this.state.isSnackbarOpen}
                            />
                        }
                    </Container>
                </Dialog>
            </div>
		);
    }

    componentWillUnmount() {
        ValidatorForm.removeValidationRule("isValidBusinessId");
    }

    private onCloseModal = () => {
        this.props.onCloseModal();
        this.setState(DEFAULT_STATE);
    }

    private handlePermissionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ 
            workingPermission: event.target.checked, 
        });
    };

    private handleAlcoholChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ 
            alcohol: event.target.checked, 
        });
    };

    private handleRobbingChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ 
            robbery: event.target.checked, 
        });
    };

    private handleConsentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ 
            privacyConsent: event.target.checked, 
        });
    };

    private handleChange = (event: ChangeEvent<any>) => {
        const {target: {name, value}} = event;

        // @ts-ignore
        this.setState({
            [name]: value,
        });
    }

    private onSnacbarVisibilityChange = (isOpen: boolean) => {
        this.setState({
            isSnackbarOpen: isOpen,
        });
    }

    private onSubmit = (event: FormEvent<Element>) => {
		event.preventDefault();
		this.setState({
			loading: true,
        });

        // Ak nemame suhlas, mneodosleme formular
        if (!this.state.privacyConsent) {
            const errors: string[] = [];
            errors.push("We must have babysitter consent agreement for adding to our database!");
            this.setState({
                errors,
                isSnackbarOpen: true,
            })
            return;
        }
        
        const {businessId, gender, workingPermission, alcohol, robbery, privacyConsent, notes} = this.state;
        const rating = this.props.getRating({workingPermission, alcohol, robbery});
        const convertedBusinessId: number = parseInt(businessId);
        const formData = {
            businessId: convertedBusinessId,
            gender,
            workingPermission,
            alcohol,
            robbery,
            privacyConsent,
            notes,
            rating,
        };

        if (this.props.selectedBabysitter) {
            this.props.onUpdateBabysitter(this.props.selectedBabysitter.screamId, formData);
        } else {
            if (businessId.length > 5) {
                this.props.onAddBabysitter(formData);
            }
        }
	};
}

export default withStyles(customStyles, { withTheme: true })(
    connect<null, null, IExternalProps>(
        null,
        null,
    )(
        AddBabysitterFormUI,
    )
);
