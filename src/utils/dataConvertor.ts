import moment from "moment";

export function arrayToObjectOnKey<T>(data: T[], key: string): {[key: string]: T} {
    const associativeArray = {};
    data.forEach((item) => {
        associativeArray[item[key]] = item;
    });

    return associativeArray;
}

export function objectToArray<T>(data: {[key: string]: T}): T[] {
    return Object.keys(data).map(index => data[index]);
}

export function formatDate(date: Date): boolean | string {
    if (!moment(date).isValid()) {
        return false;
    }

    return moment(date).format('DD/MM/YYYY');
}

export function getNameInitials(fullName: string): null | string {
    const splitted = fullName.split(" ");
    if (splitted.length === 2) {
        return `${splitted[0].charAt(0)}${splitted[1].charAt(0)}`;
    }

    return null;
}
