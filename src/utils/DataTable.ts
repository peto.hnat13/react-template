interface SelectedRow {
    index: number, 
    dataIndex: number,
}

export interface DeletedRow {
    data: SelectedRow[],
    lookup: Object,
}

export function getDataFromSelectedRows<T>(deletedRows: DeletedRow, dataCollection: T[]): T[] {
    return deletedRows.data.map(rowData => dataCollection[Object.keys(dataCollection)[rowData.index]]);
}

export function getIndexesFromSelectedRows<T>(deletedRows: DeletedRow, dataCollection: T[]): number[] {
    return deletedRows.data.map(rowData => rowData.index);
}
