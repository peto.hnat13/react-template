import Cookies from 'universal-cookie';

export default class VerificationToken {
    private token: null | string;
    private expiration: null | Date;
    private COOKIE_NAME = "verificationToken"

    constructor() {
        this.token = null;
        this.expiration = null;
    }

    set(value: string) {
        this.token = value;
    }

    get() {
        return this.token;
    }

    setExpiration(expiration: number) {
        this.expiration = new Date(new Date().getTime() + expiration);
    }

    getExpiration() {
        return this.expiration;
    }

    storeToCookie(cookieName: string, expiration: number) {
        this.getFromCookie(cookieName);
        this.setExpiration(expiration);

        if (!this.token || !this.expiration) {
            console.error("Token does not exist!")
            return;
        }

        const cookies = new Cookies()
        cookies.set(cookieName, this.token, {
            expires: this.expiration,
        });
    }

    getFromCookie(cookieName: string) {
        const cookies = new Cookies();
        this.token = cookies.get(cookieName);
    }

    isValid(): boolean {
        if (!this.expiration) {
            console.error("Token is not valid");
            return false;
        }

        return (this.expiration.getTime() - new Date().getTime()) > 0;
    }
}
