import axios, {AxiosRequestConfig} from "axios";
import { APP_CONFIG } from "app/config";
import { createUser } from "ui/organism/Dashboard/dashboardResource";
import { User, UserCredentials, RegisterCredentials } from "ui/organism/User/state";
import { createRegisteredUser } from "ui/organism/RegisteredUsers/RegisteredUsersResource";

const requestConfig: AxiosRequestConfig = {
    baseURL: APP_CONFIG.API.FIREBASE.BASE_URL, // TODO uprav BASE_URL na svoje API
    timeout: 30000,
};

// Nastavenie autorizacnych hlaviciek, mozno odmazat, ked nepouzivas vo svojich requestoch
function setAuthorizationHeaders(token): AxiosRequestConfig {
    return {
        ...requestConfig,
        headers: {
            Authorization: `Bearer ${token}`,
        },
    };
}

export async function getUsers(): Promise<any> {
    const response = await axios.get("/users", requestConfig);
    return {
        users: response.data.map(user => createRegisteredUser(user)),
    }
}

// TODO endpoint v mojom API, uprav na svoje, testovaci ucet: hnt@centrum.sk, pass: 123456
export async function login(credentials: UserCredentials): Promise<string | Error> {
    const response = await axios.post("/login", credentials, requestConfig);
    console.log("response data", response.data);

    // TODO Error handling
    if (response.data.general) {
        return new Error(response.data.general);
    }

    return response.data.token;
}

export async function getUserDetail(token: string): Promise<User> {
    const response = await axios.get("/user", setAuthorizationHeaders(token));
    console.log(response);
    return createUser(response.data);
}

export async function signup(credentials: RegisterCredentials): Promise<any> {
    const response = await axios.post("/signup", credentials, requestConfig);
    // TODO Error handling
    return response.data;
}
