export const APP_CONFIG = {
    IMAGES: {
        ATOM: {
            PATH: "../ui/atom",
        },
        ORGANISM: {
            PATH: "../ui/organism",
        },
    },
    COMMON: {
        IMAGES: {
            PATH: "../images",
        },
    },
    API: {
        FIREBASE: {
            BASE_URL: "https://us-central1-socialapp-bb16f.cloudfunctions.net/v2",
        },
    },
    USER: {
        ROLES: {
            ADMIN: "admin",
            GUEST: "guest",
            REGISTERED: "registered",
        },
    },
    FORM: {
        MESSAGE: {
            TYPES: {
                SUCCESS: "success",
                ERROR: "error",
                INFO: "info",
            }
        },
        REGISTRATION: {
            SUCCESS_MESSAGE: "Ďakujeme Vám za registráciu, email s inštrukciami Vám bol poslaný na zadanú emailovú adresu. Nedostali ste email? Skúste ho odoslať znova kliknutím na tlačidlo nižšie."
            // TODO pridanie dalsich typov messages
        }
    }
};
