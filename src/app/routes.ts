import { ConnectedComponent } from "react-redux";
import {RouteConfig, RouteConfigComponentProps} from "react-router-config";
import DashboardUI from "ui/organism/Dashboard/Dashboard";
import HomeUI from "ui/organism/Home/Home";
import LoginUI from "ui/organism/Login/Login";
import SignupUI from "ui/organism/Signup/Signup";

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>
type Merge<M, N> = Omit<M, Extract<keyof M, keyof N>> & N;

// redefine name to be string | number
export type connectedComponentRouteConfig = Merge<RouteConfig, {
    component?: React.ComponentType<RouteConfigComponentProps<any>> | React.ComponentType | ConnectedComponent<any, any>;
}>;

export const ROUTE = {
    HOME: "/",
    LOGIN: "/login",
    SIGNUP: "/signup",
    DASHBOARD: "/dashboard",
};

export const routes: connectedComponentRouteConfig[] = [
    {
        path: ROUTE.HOME,
        exact: true,
        component: HomeUI,
    },
    {
        path: ROUTE.LOGIN,
        exact: true,
        component: LoginUI,
    },
    {
        path: ROUTE.SIGNUP,
        exact: true,
        component: SignupUI,
    },
];

export const protectedRoutes: connectedComponentRouteConfig[] = [
    {
        path: ROUTE.DASHBOARD,
        exact: true,
        component: DashboardUI,
    },
];
