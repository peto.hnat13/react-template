import { combineReducers} from "redux";
import applicationReducer from "ui/organism/App/reducer";
import dashboardReducer from "ui/organism/Dashboard/reducer";
import userReducer from "ui/organism/User/reducer";
import signupFormReducer from "ui/organism/Signup/reducer";
import { IState as AppState, NAMESPACE as APP_NAMESPACE} from "ui/organism/App/state";
import { IState as DashboardState, NAMESPACE as DASHBOARD_NAMESPACE} from "ui/organism/Dashboard/state";
import { IState as UserState, NAMESPACE as USER_NAMESPACE} from "ui/organism/User/state";
import { IState as SignupFormState, NAMESPACE as SIGNUP_NAMESPACE} from "ui/organism/Signup/state";

export interface State {
    [APP_NAMESPACE]: AppState;
    [DASHBOARD_NAMESPACE]: DashboardState;
    [USER_NAMESPACE]: UserState;
    [SIGNUP_NAMESPACE]: SignupFormState;
}

export const rootReducer = combineReducers<State>({
    [APP_NAMESPACE]: applicationReducer,
    [DASHBOARD_NAMESPACE]: dashboardReducer,
    [USER_NAMESPACE]: userReducer,
    [SIGNUP_NAMESPACE]: signupFormReducer,
    // TODO: add another reducers
});